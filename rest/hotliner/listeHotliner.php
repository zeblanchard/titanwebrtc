<html>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <head>
        <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            /*width: 100%;*/
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: center;
            margin: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        #nouveau {
            margin : 8px;
        }
        #td["value"="1"]
        {
            background-color: #dddddd;
        }
        </style>
    </head>
    <body>

        <?php
        require("./controllerHotliner.php");
        $controllerSalle=new controllerHotliner();
        if(isset($_POST["idhotlineur"]))
        {
            if(isset($_POST["actif"]))
            {
                $actif=true;
            }
            else {
                $actif=false;
            }
           $request=$controllerSalle->MajHotlineur($_POST["idhotlineur"],$_POST["nom"],$_POST["prenom"],$_POST["mail"],$_POST["tel"], $actif);
        }
        else if(isset($_GET["idhotlinerDelete"])){
            $request=$controllerSalle->DeleteHotlineur($_GET["idhotlinerDelete"]);
        }
        else {
         $request=$controllerSalle->getListeHotlineur();
        }
        

        ?>
                    <button id="nouveau" class="btn btn-primary" onclick="window.location.href='./hotliner.php'" >Nouveau Hotliner</button>
            <table class="table">
            <thead>
                <tr>
                    <th scope="col" style="display:none;">Id</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Prenom</th>
                    <th scope="col">Mail</th>
                    <th scope="col">Telephone</th>
                    <th scope="col">Actif</th>
                    <th scope="col" ></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php
            if(sizeof($request)>0)
            {
            foreach ($request as $row){
            ?>
            <?php if($row["actif"]==true) { ?>
                <tr  class="table-success">
            <?php } else { ?>
                <tr class="table-warning">
            <?php } ?>
                <td  style="display:none;"><?php echo(str_replace('.','/',$row["idhotlineur"])) ?></td>
                <td><?php echo($row["nom"]) ?></td>
                <td><?php echo($row["prenom"]) ?></td>
                <td><?php echo($row["mail"]) ?></td>
                <td><?php echo($row["telephone"]) ?></td>
                <td id="actif"><?php if($row["actif"]==true){ echo("1");}else echo("0"); ?></td>
                <td><button class="btn btn-secondary"
                    onclick=
                        "window.location.href='./hotliner.php?idhotliner=<?php echo(urlencode($row['idhotlineur'])) ?>&nom=<?php echo(urlencode($row['nom'])) ?>&prenom=<?php echo(urlencode($row['prenom'])) ?>&telephone=<?php echo(urlencode($row['telephone'])) ?>&mail=<?php echo(urlencode($row['mail'])) ?>&actif=<?php echo(urlencode($row['actif'])) ?>'">Modifier</button>
                </td>
                <td><button class="btn btn-danger"
                    onclick=
                        "window.location.href='./listeHotliner.php?idhotlinerDelete=<?php echo(urlencode($row['idhotlineur'])) ?>'">Supprimer</button>
                </td>
            </tr>
            <?php
            }
            }
            ?>
            </tbody>

            </table>
            <?php //phpinfo(); ?>
    </body>
</html>
