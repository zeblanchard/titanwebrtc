<?php
class controllerHotliner
{
    //private $_webservice="http://192.168.1.73/titanwebrtc/rest/wsvisio.php";
    private $_webservice="http://update.malta-informatique.fr/WS/Visio/wsvisio.php";

    public function __construct()
    {
    }

    public function getListeHotlineur()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_webservice."?hotlineList");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain'));
        
        $result = curl_exec($ch);
        curl_close($ch);
        return (json_decode($result,true));
        
        //return null;
    }

    public function MajHotlineur($idhotlineur,$nom,$prenom,$mail,$telephone,$actif)
    {
        $ch = curl_init();
        if($idhotlineur!=0)
        {
            curl_setopt($ch, CURLOPT_URL, $this->_webservice."?hotlineUpdate");
        }
        else 
        {
            curl_setopt($ch, CURLOPT_URL, $this->_webservice."?hotlineNew");

        }
        $data[0]["Idhotlineur"]=$idhotlineur;

        $data[0]["Actif"]=$actif;
        $data[0]["Nom"]=$nom;
        $data[0]["Prenom"]=$prenom;
        $data[0]["Mail"]=$mail;
        $data[0]["Telephone"]=$telephone;


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE));

        $result = curl_exec($ch);
        curl_close($ch);
        return (json_decode($result,true));
    }
    public function DeleteHotlineur($idhotlineur)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_webservice."?hotlineDelete");
        $data[0]["Idhotlineur"]=$idhotlineur;

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE));
        $result = curl_exec($ch);
        curl_close($ch);
        return (json_decode($result,true));
    }

}



