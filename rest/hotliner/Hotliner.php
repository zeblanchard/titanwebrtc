<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<style>

</style>

<?php
        if(isset($_GET["idhotliner"]))
        {
            $row["idhotlineur"]=$_GET["idhotliner"];
            $row["nom"]=$_GET["nom"];
            $row["prenom"]=$_GET["prenom"];
            $row["telephone"]=$_GET["telephone"];
            $row["mail"]=$_GET["mail"];
            $row["actif"]=$_GET["actif"];
        }
        else {
            $row["idhotlineur"]="0";
            $row["nom"]="";
            $row["prenom"]="";
            $row["telephone"]="";
            $row["mail"]="";
            $row["actif"]=true;
        }
?>  
        <div style="margin:20px;">
            <form action="./listeHotliner.php" method="POST">
                <input name="idhotlineur" id="idhotlineur" type="hidden" value=<?php echo(urldecode($row["idhotlineur"])) ?>>
                <div class="form-group row">
                    <label for="nom" class="col-sm-1 col-form-label">Nom</label>
                    <div class="col-sm-5">
                    <input name="nom" placeholder="Nom" class="form-control" id="nom" type="text" value=<?php echo(urldecode($row["nom"])) ?>>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="prenom" class="col-sm-1 col-form-label">Prenom</label>
                    <div class="col-sm-5">
                    <input name="prenom" placeholder="Prenom" class="form-control" id="prenom" type="text" value=<?php echo(urldecode($row["prenom"])) ?>>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tel" class="col-sm-1 col-form-label">Téléphone</label>
                    <div class="col-sm-5">
                     <input name="tel" placeholder="Téléphone" class="form-control" id="tel" type="text" value=<?php echo(urldecode($row["telephone"])) ?>>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mail" class="col-sm-1 col-form-label">Mail</label>
                    <div class="col-sm-5">
                    <input name="mail" placeholder="Mail" class="form-control" id="mail" type="text" value=<?php echo(urldecode($row["mail"])) ?>>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-1">Actif</div>
                    <div class="col-sm-5">
                        <div class="form-check">
                            <input name="actif" class="form-check-input" id="actif" value="1" type="checkbox" <?php if($row["actif"]==true){?>checked<?php } ?>>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                        <input type='submit'  class="btn btn-primary col-sm-6" value='Enregistrer'/>
                </div>
            </form>
        </div>