<?php header("Access-Control-Allow-Origin: *");

require('class/Salle.php');
require('class/LogSalle.php');
require('class/HotlineInfo.php');
require('class/ServiceEnvoie.php');

if(isset($_GET["creerConsultation"])) {
    $template="template_email";
    if(isset($_GET["template"])) {
        $template= $_GET["template"];
    }

    $tab= json_decode(file_get_contents("php://input"),true);
    $salle = new Salle($tab);
    $salle->creeSalle();
    if($salle->geturl()==''){
        $tabretour['statut']='Error';
        $tabretour['url']='';
}
else
{   
        $salle->envoiMail( $template);
        $salle->envoieSms();
    //}
    $tabretour['statut']='ok';
    $tabretour['url']=$salle->geturl();
}
$logSalle=new LogSalle();
$logSalle->createSalle($salle);

header('Content-Type: application/json; charset: UTF-8');
echo json_encode($tabretour);
}


if(isset($_GET["debutConsultation"])) {
    $tab= json_decode(file_get_contents("php://input"),true);
    $logSalle=new LogSalle();
    $rpps=$tab[0]["RPPS"];
    $n_dossier=$tab[0]["n_dossier"];
    $finess=$tab[0]["Finess"];
    $idteleconsultationencours=$tab[0]["IdTeleconsultationEnCours"];

    $logSalle->debutConsultation($rpps,$n_dossier,$finess,$idteleconsultationencours);
    }

if(isset($_GET["finConsultation"])) {
    $tab= json_decode(file_get_contents("php://input"),true);
    $logSalle=new LogSalle();
    $rpps=$tab[0]["RPPS"];
    $n_dossier=$tab[0]["n_dossier"];
    $finess=$tab[0]["Finess"];
    $logSalle->finConsultation($rpps,$n_dossier,$finess);
    }

if(isset($_GET["infoConsultation"]))
{
    $tab= json_decode(file_get_contents("php://input"),true);
    $logSalle=new LogSalle();
    $rpps=$tab[0]["RPPS"];
    $n_dossier=$tab[0]["n_dossier"];
    $finess=$tab[0]["Finess"];

    $logSalle=new LogSalle();
    $tabRequete=$logSalle->infoSalle($rpps,$n_dossier,$finess);
    $tabResult[0]["IdConsultationLog"]=$tabRequete["idconsultation"];
    $tabResult[0]["Idprescrip"]=$tabRequete["idprescrip"];
    $tabResult[0]["MedecinMail"]=$tabRequete["mail"];
    $tabResult[0]["Medecin"]=$tabRequete["medecin"];
    $tabResult[0]["MedecinPortable"]=$tabRequete["telephone"];
    $tabResult[0]["n_dossier"]=$tabRequete["n_dossier"];
    $tabResult[0]["Patient"]=$tabRequete["patient"];
    $tabResult[0]["RPPS"]=$tabRequete["rpps"];
    $tabResult[0]["Consentement"]=$tabRequete["consentement"];
    $tabResult[0]["Finess"]=$tabRequete["finess"];
    $tabResult[0]["Etablissement"]=$tabRequete["etablissement"];
    $tabResult[0]["UrlWebMedecin"]=$tabRequete["urlWebMedecin"];
    $tabResult[0]["Accompagnant"]=$tabRequete["accompagnant"];
    $tabResult[0]["UrlPatient"]=$tabRequete["url"];
    $tabResult[0]["Date"]=$tabRequete["dateJour"];
    $tabResult[0]["IdUtilisateur"]=$tabRequete["idUtilisateur"];
    $tabResult[0]["TelEtablissement"]=$tabRequete["telEtablissement"];
    if($tabRequete["idTeleconsultationEnCours"]==null)
    {
        $tabResult[0]["IdTeleconsultationEnCours"]=0;
    }
    else {
        $tabResult[0]["IdTeleconsultationEnCours"]=$tabRequete["idTeleconsultationEnCours"];
    }
    header('Content-Type: application/json; charset: UTF-8');
    echo json_encode($tabResult);
}

if(isset($_GET["logConsultation"]))
{
    $tab= json_decode(file_get_contents("php://input"),true);
    $logSalle=new LogSalle();
    if(isset($tab[0]["idconsultation"]))
    {
        $idconsultation=$tab[0]["idconsultation"];
        $tabRequete=$logSalle->logSalleWithId($idconsultation);

    }
    else {
        $rpps=$tab[0]["RPPS"];
        $n_dossier=$tab[0]["n_dossier"];
        $finess=$tab[0]["Finess"];
        $tabRequete=$logSalle->logsSalle($rpps,$n_dossier,$finess);
    }


    
    header('Content-Type: application/json; charset: UTF-8');
    echo json_encode($tabRequete);
}
if(isset($_GET["listeSalle"]))
{
    $logSalle=new LogSalle();
    $tab= json_decode(file_get_contents("php://input"),true);
    if($tab==null)
    {
        $tabRequete=$logSalle->listeSalleAll();
    }
    else {
        $date=$tab[0]["Date"];
        $tabRequete=$logSalle->listeSalle($date);
    }

    header('Content-Type: application/json; charset: UTF-8');
    echo json_encode($tabRequete);
    
}

if(isset($_GET["planifieConsultation"]))
{
    $tab= json_decode(file_get_contents("php://input"),true);
    if($tab[0]["Template"]!=null && $tab[0]["Template"]!="")
    {
        $template=$tab[0]["Template"];
    }
    else
    {
        $template="template_email_planification";
    }
    $variablesARemplacer = array(
        '%NOM_ETABLISSEMENT%',
        '%URL_TWM%',
        '%RESIDENT%',
        '%TEL%',
        '%DATE%',
        "%HEURE%"
    );
        $variablesARemplacerPar = array(
        $tab[0]["Nom_Etable"], 
        $tab[0]["UrlWebMedcecin"]."?rpps=".urlencode($tab[0]["RPPS"])."&finess=".urlencode($tab[0]["Finess"]), 
        $tab[0]["NomResident"], 
        $tab[0]["TElEtab"],
        $tab[0]["Date"],
        $tab[0]["Heure"]
    );
    $email =$tab[0]["MailMed"];
    $subject="Téléconsultation planifiée pour " . $tab[0]["NomResident"] . " (" . $tab[0]["Nom_Etable"] . ")";
    
    ServiceEnvoie::EnvoieMail($subject,$email,$template,$variablesARemplacer,$variablesARemplacerPar,'Téléconsultation Titan');
}

if(isset($_GET["getToken"])) {
    $tabResult[0]["apzkey"]="apzkey:213f43c798fde161928ea3ba69ec1fd5";
    $tabResult[0]["cloudUrl"]="https://hds.apizee.com";
    $tabResult[0]["ccs"]="ccs-hds.apizee.com";
    $tabResult[0]["ScriptUrl"]="https://cloud.apizee.com/apiRTC/v4.2/apiRTC-latest.min.js";
    header('Content-Type: application/json; charset: UTF-8');
    echo json_encode($tabResult);
}

if(isset($_GET["sendRappelSupport"]))
{
    $tab= json_decode(file_get_contents("php://input"),true);
    if(isset($tab[0]))
    {
        ServiceEnvoie::EnvoieSupport($tab[0]);
    }
}
if(isset($_GET["hotlineNew"]))
{
    $hotlineInfo= new HotlineInfo;
    $tab= json_decode(file_get_contents("php://input"),true);
    if(isset($tab[0]))
    {
        if(isset($tab[0]["Nom"]) && isset($tab[0]["Nom"]) && isset($tab[0]["Prenom"]) && isset($tab[0]["Mail"]) && isset($tab[0]["Telephone"])  && isset($tab[0]["Actif"]) )
        {
            $tabRequete=$hotlineInfo->newHotlineur($tab[0]["Nom"],$tab[0]["Prenom"],$tab[0]["Mail"],$tab[0]["Telephone"],$tab[0]["Actif"]);
            header('Content-Type: application/json; charset: UTF-8');
            echo json_encode($tabRequete); 
        }
        else {
            die("ERREUR INFO INCOMPLETE");
        }
    }
}
if(isset($_GET["hotlineUpdate"]))
{
    $hotlineInfo= new HotlineInfo;
    $tab= json_decode(file_get_contents("php://input"),true);
    if(isset($tab[0]))
    {
        if(isset($tab[0]["Idhotlineur"]) && isset($tab[0]["Nom"]) && isset($tab[0]["Nom"]) && isset($tab[0]["Prenom"]) && isset($tab[0]["Mail"]) && isset($tab[0]["Telephone"])  && isset($tab[0]["Actif"]) )
        {
            $tabRequete=$hotlineInfo->updateHotlineur($tab[0]["Idhotlineur"],$tab[0]["Nom"],$tab[0]["Prenom"],$tab[0]["Mail"],$tab[0]["Telephone"],$tab[0]["Actif"]);
            header('Content-Type: application/json; charset: UTF-8');
            echo json_encode($tabRequete); 
        }
        else {
            die("ERREUR INFO INCOMPLETE");
        }
    }
}

if(isset($_GET["hotlineDelete"]))
{
    $hotlineInfo= new HotlineInfo;
    $tab= json_decode(file_get_contents("php://input"),true);
    if(isset($tab[0]))
    {
        if(isset($tab[0]["Idhotlineur"]))
        {
            $tabRequete=$hotlineInfo->deleteHotlineur($tab[0]["Idhotlineur"]);
            header('Content-Type: application/json; charset: UTF-8');
            echo json_encode($tabRequete); 
        }
        else {
            die("ERREUR INFO INCOMPLETE");
        }
    }
}

if(isset($_GET["hotlineList"]))
{
    $hotlineInfo= new HotlineInfo;
    $tabRequete=$hotlineInfo->listeHotlineur();
    header('Content-Type: application/json; charset: UTF-8');
    echo json_encode($tabRequete); 
}

