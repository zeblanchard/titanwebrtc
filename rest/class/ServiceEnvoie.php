<?php
require 'mailjet/vendor/autoload.php';
use \Mailjet\Resources;
class ServiceEnvoie
{
private Static $AUTHORIZED_IP = array("82.240.45.23","109.190.94.78","37.58.176.54","92.154.29.239","::1");

private Static $AUTHORIZED_MAIL = array(
    "gregoire.derotalier@malta-informatique.fr", //gregoire
    //DEV
    "nicolas.allard@malta-informatique.fr", //nicolas
    "david.delforge@malta-informatique.fr", //david
    "thomas.blanchard@malta-informatique.fr", //thomas
    "pauline.teyssou@malta-informatique.fr", //pauline
    "benoit.pinto@malta-informatique.fr", //benoit
    "nicolas.vincent@malta-informatique.fr", //nicolas V
    //COMMERCIAUX
    "yves.prevost@malta-informatique.fr", //Yves PREVOST 	
    "nicolas.brenaud@malta-informatique.fr", //Nicolas BRENAUD
    "olivier.camin@malta-informatique.fr", //Olivier CAMIN
    "bruno.caron@malta-informatique.fr", //Bruno CARON
    "julien.dourliach@malta-informatique.fr", //Julien DOURLIACH
    "thomas.ducos@malta-informatique.fr", //Thomas DUCOS
    "rafael.duplaga@malta-informatique.fr", //Rafaël DUPLAGA
    "alexis.hoyez@malta-informatique.fr", //Alexis HOYEZ
    "joseph.kardous@malta-informatique.fr", //Joseph KARDOUS
    "manon.lesimple@malta-informatique.fr", //Manon LESIMPLE
    "mathias.milon@malta-informatique.fr", //Mathias MILON
    "damien.corre@malta-informatique.fr", //Damien CORRE
    //HIT
    "medecin@malta-informatique.fr" //MEDECIN

);
private Static $AUTHORIZED_PHONE = array(
    "0675882282", //gregoire
    //DEV
    "0633405279", //nicolas A
    "0638413897", //thomas 
    "0664811364", //david
    "0685043938", //Pauline
    "0632176704", //Nicolas V
    //COMERCIAUX
    "0695760515",//Yves PREVOST 
    "0782270768",//Nicolas BRENAUD
    "0683643577",//Olivier CAMIN 
    "0767156150",//Bruno CARON
    "0667511260",//Julien DOURLIACH
    "0786515292",//Thomas DUCOS 
    "0667276622",//Rafaël DUPLAGA
    "0681048775",//Alexis HOYEZ 
    "0760727806",//Joseph KARDOUS
    "0556111561",//Manon LESIMPLE
    "0762210706",//Mathias MILON
    "0699342866" //Damien CORRE
);

private Static $SUPPORT_PHONE=array(
"0633405279", //NICOLAS
//"0685043938" //pauline
);
private Static $SUPPORT_MAIL=array(
    "nicolas.allard@malta-informatique.fr", //nicolas
 //   "pauline.teyssou@malta-informatique.fr" //pauline
);
    

 public static function EnvoieMail($p_subject,$p_email,$p_template,$p_variableRemplacer,$p_variableRemplacerPar,$p_from){
    if (in_array($p_email,ServiceEnvoie::$AUTHORIZED_MAIL) || in_array($_SERVER['REMOTE_ADDR'],ServiceEnvoie::$AUTHORIZED_IP)==false)
    {

        $emailTXT = file_get_contents("./template/". $p_template.".txt",true);
        $emailHTML = file_get_contents("./template/". $p_template.".html",true);
        $emailTXT = str_replace($p_variableRemplacer, $p_variableRemplacerPar, $emailTXT);
        $emailHTML = str_replace($p_variableRemplacer, $p_variableRemplacerPar, $emailHTML);
    
        ServiceEnvoie::SendMail($p_from,$p_subject,$emailTXT,$emailHTML,$p_email);
    }
 }

    private static function SendMail($p_from,$p_subject,$emailTXT,$emailHTML,$p_email)
    {
        $mj = new \Mailjet\Client('15cb76eab8d63772a70ccbf76fe553d0', '966da8109dbe65df3a87ff858d1599e0');
        $body = [
            'FromEmail' => "no-reply@malta-informatique.fr",
            'FromName' => $p_from,
            'Subject' => $p_subject,
            'Text-part' => $emailTXT,
            'Html-part' => $emailHTML,
            'Recipients' => [
                [
                    'Email' => $p_email
                ]
            ]
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);
    }
     public static function EnvoieSMS($p_telephone,$p_message,$garderSautLigne=false){
        if (in_array($p_telephone,ServiceEnvoie::$AUTHORIZED_PHONE) || in_array($_SERVER['REMOTE_ADDR'],ServiceEnvoie::$AUTHORIZED_IP)==false)
        {
            $APItoken = "yNASXyr3PP0Olkmom1pKI5gg4HDFe5G4";
            if($garderSautLigne==false)
            {
                $caractereARemplacer = array('%', '\n', '\r', ' ', '!', '“', '#', '$', '&', '‘', '(', ')', '*', ',', '-', '.', '/', '{', '|', '}', '~', '[', '\\', ']', '^', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', '¡', '£', '€', '¥', '§', '¿', 'Ä', 'Å', 'Æ', 'Ç', 'è', 'É', 'Ñ', 'Ö', 'Ø', 'Ü', 'ß', 'à', 'ä', 'å', 'æ', 'è', 'é', 'ì', 'ñ', 'ò', 'ö', 'ø', 'ù', 'Ü', '€');
                $caractereARemplacerPar = array('%25', '%0A', '%0D', '+', '%21', '%22', '%23', '%24', '%26', '%27', '%28', '%29', '*', '%2C', '-', '.', '%2F', '%7B', '%7C', '%7D', '%7E', '%5B', '%5C', '%5D', '%5E', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '%3A', '%3B', '%3C', '%3D', '%3E', '%3F', '%40', '%A1', '%A3', '%A4', '%A5', '%A7', '%BF', '%C4', '%C5', '%C6', '%C7', '%C8', '%C9', '%D1', '%D6', '%D8', '%DC', '%DF', '%E0', '%E4', '%E5', '%E6', '%E8', '%E9', '%EC', '%F1', '%F2', '%F6', '%F8', '%F9', '%FC', '%80');    
            }
            else {
                $caractereARemplacer = array('\n', '\r', ' ', '!', '“', '#', '$', '&', '‘', '(', ')', '*', ',', '-', '.', '/', '{', '|', '}', '~', '[', '\\', ']', '^', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', '¡', '£', '€', '¥', '§', '¿', 'Ä', 'Å', 'Æ', 'Ç', 'è', 'É', 'Ñ', 'Ö', 'Ø', 'Ü', 'ß', 'à', 'ä', 'å', 'æ', 'è', 'é', 'ì', 'ñ', 'ò', 'ö', 'ø', 'ù', 'Ü', '€');
                $caractereARemplacerPar = array('%0A', '%0D', '+', '%21', '%22', '%23', '%24', '%26', '%27', '%28', '%29', '*', '%2C', '-', '.', '%2F', '%7B', '%7C', '%7D', '%7E', '%5B', '%5C', '%5D', '%5E', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '%3A', '%3B', '%3C', '%3D', '%3E', '%3F', '%40', '%A1', '%A3', '%A4', '%A5', '%A7', '%BF', '%C4', '%C5', '%C6', '%C7', '%C8', '%C9', '%D1', '%D6', '%D8', '%DC', '%DF', '%E0', '%E4', '%E5', '%E6', '%E8', '%E9', '%EC', '%F1', '%F2', '%F6', '%F8', '%F9', '%FC', '%80');    
            }

            $message = str_replace($caractereARemplacer, $caractereARemplacerPar, $p_message);
            if(strlen($message)>158)
            {
                $message=substr($message,0,158);
                if($message{strlen($message-1)}=="%")
                {
                    $message=substr($message,0,157);
                }
                else {
                    if($message{strlen($message)-2}=="%")
                        {
                            $message=substr($message,0,156);
                        }
                }
            }
            //TODO: enlevé numéro en dure
            //numero en dure pour eviter le spam
            $sSMSModeUrl = "https://api.smsmode.com/http/1.6/sendSMS.do?accessToken=" . $APItoken . "&message=" . $message . "&numero=" . $p_telephone;
            file_get_contents($sSMSModeUrl);
        }
    }
    private static function EnvoieMailSupport($message,$tabMail)
    {
        foreach ($tabMail as $mail){
            $messageTemp=$message;
            ServiceEnvoie::SendMail("RAPPEL IMMEDIAT WEB MEDECIN","RAPPEL IMMEDIAT WEB MEDECIN",$messageTemp,"", $mail);
        }
    }
    private static function EnvoiSMSSupport($message,$tabNumero)
    {
        foreach ($tabNumero as $phone){
            $messageTemp=$message;
           ServiceEnvoie::EnvoieSMS($phone,$messageTemp,true);
        }
    }
    public static function EnvoieSupport($tab)
    {
        $infoHotline=new HotlineInfo();
        $row=$infoHotline->listeTelephoneMailActif();
        $nbHotlineMail=0;
        $nbHotlineTelephone=0;

        foreach ($row as $hotlineur){
            if($hotlineur["mail"]!="")
            {
                $tabMail[$nbHotlineMail]=$hotlineur["mail"];
                $nbHotlineMail++;    
            }
            if($hotlineur["telephone"]!="")
            {
                $tabTelephone[$nbHotlineTelephone]=$hotlineur["telephone"];
                $nbHotlineTelephone++;
            }
        }

        $messageTel;
        $messageMail;
        if(isset($tab["Telephone"]) && $tab["Telephone"]!='' && ($nbHotlineMail>0 || $nbHotlineTelephone>0) )
        {
            $messageTel="RAPPEL IMMEDIAT WEBMEDECIN : %0A";
            $messageTel.="TEL : ".$tab["Telephone"]."%0A";
            $messageTel.="RPPS : ".$tab["RPPS"]."%0A";
            $messageTel.="FINESS : ".$tab["Finess"]."%0A";
            $messageTel.="NOM : ".$tab["Nom"]."%0A";
            $messageTel.="PRENOM : ".$tab["Prenom"]."%0A";
           // $messageTel.="URL : ".$tab["URL"]."%0A";

            $messageMail="RAPPEL IMMEDIAT WEBMEDECIN : 
            TEL : ".$tab["Telephone"]."
            RPPS : ".$tab["RPPS"]."
            FINESS : ".$tab["Finess"]."
            NOM : ".$tab["Nom"]."
            PRENOM : ".$tab["Prenom"]."
            URL : ".$tab["URL"];
            if($nbHotlineMail>0)
            {
               ServiceEnvoie::EnvoieMailSupport($messageMail,$tabMail);
            }
            if($nbHotlineTelephone>0)
            {
                ServiceEnvoie::EnvoiSMSSupport($messageTel,$tabTelephone);
            }

        }
    }
}