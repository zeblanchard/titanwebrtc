<?php
class Salle
{
    const URL_DEFAULT="https://visioconf.titanweb.fr";
    private $_idprescrip;
    private $_url = '';
    private $_libelle = '';
    private $_rpps;
    private $_mail = '';
    private $_telephone = '';
    private $_error;
    private $_medecin;
    private $_patient;
    private $_consentement;
    private $_finess;
    private $_etablissement;
    private $_urlWebMedecin;
    private $_accompagnant;
    private $_n_dossier;
    private $_statut;
    private $_idutilisateur;
    private $_telEtablissement;
    private $_idteleconsultationEnCours;
    private $_urlPerso ='';
    public function __construct($tab)
    {

        if (isset($tab["0"]["Idprescrip"])) {
            $this->_idprescrip = $tab["0"]["Idprescrip"];
        } else {
            $this->_error = "Idprescrip";
        }

        if (isset($tab["0"]["RPPS"])) {
            $this->_rpps = $tab["0"]["RPPS"];
        } else {
            $this->_error = "RPPS";
        }

        if (isset($tab["0"]["MedecinNom"]) && isset($tab["0"]["MedecinPrenom"])) {
            $this->_medecin = $tab["0"]["MedecinNom"] . ' ' . $tab["0"]["MedecinPrenom"];
        } else {
            $this->_error = "MedecinNom MedecinPrenom";
        }

        if (isset($tab["0"]["MedecinPortable"])) {
            $replace = array(" ", ".");
            $this->_telephone = str_replace($replace, "", $tab["0"]["MedecinPortable"]);
        } else {
            $this->_error = "MedecinPortable";
        }

        if (isset($tab["0"]["MedecinMail"])) {
            $this->_mail = $tab["0"]["MedecinMail"];
        } else {
            $this->_error = "MedecinMail";
        }

        if (isset($tab["0"]["PatientNom"]) && isset($tab["0"]["PatientPrenom"])) {
            $this->_patient = $tab["0"]["PatientNom"] . ' ' . $tab["0"]["PatientPrenom"];
        } else {
            $this->_error = "PatientNom PatientPrenom";
        }

        if (isset($tab["0"]["Consentement"])) {
            if ($tab["0"]["Consentement"] == 1) {
                $this->_consentement = true;
            } else {
                $this->_consentement = false;
            }
        } else {
            $this->_error = "Consentement";
        }

        if (isset($tab["0"]["Finess"])) {
            $this->_finess = $tab["0"]["Finess"];
        } else {
            $this->_error = "Finess";
        }

        if (isset($tab["0"]["Etablissement"])) {
            $this->_etablissement = $tab["0"]["Etablissement"];
        } else {
            $this->_error = "Etablissement";
        }

        if (isset($tab["0"]["UrlWebMedecin"])) {
            $this->_urlWebMedecin = $tab["0"]["UrlWebMedecin"];
        } else {
            $this->_error = "UrlWebMedecin";
        }

        if (isset($tab["0"]["AccompagnantNom"]) && isset($tab["0"]["AccompagnantPrenom"]) && isset($tab["0"]["AccompagnantSpecilialite"])) {
            $this->_accompagnant = $tab["0"]["AccompagnantNom"] . ' ' . $tab["0"]["AccompagnantPrenom"] . " (" . $tab["0"]["AccompagnantSpecilialite"] . ")";
        } else {
            $this->_error = "AccompagnantNom AccompagnantPrenom";
        }
        if (isset($tab["0"]["n_dossier"])) {
            $this->_n_dossier = $tab["0"]["n_dossier"];
        } else {
            $this->_error = "UrlWebMedecin";
        }

        if (isset($tab["0"]["IdUtilisateur"])) {
            $this->_idutilisateur = $tab["0"]["IdUtilisateur"];
        } else {
            $this->_error = "IdUtilisateur";
        }
        if (isset($tab["0"]["TelEtablissement"])) {
            $this->_telEtablissement = $tab["0"]["TelEtablissement"];
        } else {
            $this->_error = "TelEtablissement";
        }

        if (isset($tab["0"]["IdTeleconsultationEnCours"])) {
            $this->_idteleconsultationEnCours = $tab["0"]["IdTeleconsultationEnCours"];
        } else {
            $this->_error = "IdTeleconsultationEnCours";
        }

        //permet l'envoie de l'url perso de la tablette si saisie
        if (isset($tab["0"]["UrlPerso"])) {
            $this->_urlPerso = $tab["0"]["UrlPerso"];
        }
    }

    
    public function gettelEtablissement()
    {
        return $this->_telEtablissement;
    }
    public function getidteleconsultationEnCours()
    {
        return $this->_idteleconsultationEnCours;
    }
    public function geturl()
    {
        return $this->_url;
    }

    public function getrppss()
    {
        return $this->_rpps;
    }

    public function getndossier()
    {
        return $this->_ndossier;
    }

    public function getn_dossier()
    {
        return $this->_n_dossier;
    }

    public function getfiness()
    {
        return $this->_finess;
    }

    public function getidprescrip()
    {
        return $this->_idprescrip;
    }

    public function getlibelle()
    {
        return $this->_libelle;
    }

    public function getmail()
    {
        return $this->_mail;
    }

    public function gettelephone()
    {
        return $this->_telephone;
    }

    public function getmedecin()
    {
        return $this->_medecin;
    }

    public function getpatient()
    {
        return $this->_patient;
    }

    public function getconsentement()
    {
        return $this->_consentement;
    }

    public function getetablissement()
    {
        return $this->_etablissement;
    }

    public function geturlWebMedecin()
    {
        return $this->_urlWebMedecin;
    }

    public function getaccompagnant()
    {
        return $this->_accompagnant;
    }

    public function envoiMail($template)
    {
        if ($this->_mail !='') {
                $variablesARemplacer = array('%NOM_ETABLISSEMENT%', '%URL_TWM%', '%RESIDENT%', '%EFFECTEUR%','%TEL%');
                $variablesARemplacerPar = array($this->_etablissement, $this->_urlWebMedecin."?rpps=".urlencode($this->_rpps)."&finess=".urlencode($this->_finess), $this->_patient, $this->_accompagnant,$this->_telEtablissement);
                $email = $this->_mail;
                $subject="Téléconsultation en attente pour " . $this->_patient . " (" . $this->_etablissement . ")";
                ServiceEnvoie::EnvoieMail( $subject,$email,$template,$variablesARemplacer,$variablesARemplacerPar,'Téléconsultation Titan');
        }
    }

    public function creeSalle()
    {
        if ($this->_url == "") {
            $roomName = base64_encode($this->_rpps . '-' . $this->_finess);
            // $this->_url="https://update.malta-informatique.fr/VISIO/html/patient.html?room=".$roomName;
            //$this->_url="https://update.malta-informatique.fr/VISIO/html/patient.html?room=malta";
            //$this->_url="https://visioconf.titanweb.fr/html/patient.html?room=malta&mode=patient&username=".urlencode((String)$this->_n_dossier."|".$this->_patient);
            $urlUtilise=Salle::URL_DEFAULT;

            if($this->_urlPerso!='')
            {
                $urlUtilise=$this->_urlPerso;
            }
            $this->_url = $urlUtilise."/html/patient.html?room=" . urlencode($roomName) . "&mode=patient&username=" . urlencode((String)$this->_n_dossier . "|" . $this->_patient)."&telMedecin=".urlencode($this->_telephone);
        }

    }

    public function envoieSms()
    {
        if ($this->_telephone != '') {
                $message = "Vous avez une téléconsultation en attente pour " . $this->_patient . " (" . $this->_etablissement . " : ".$this->_telEtablissement.")";
                $destinataire = $this->_telephone;
                ServiceEnvoie::EnvoieSMS($destinataire,$message);
        }
    }

    /**
     * @return mixed
     */
    public function getIdutilisateur()
    {
        return $this->_idutilisateur;
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

