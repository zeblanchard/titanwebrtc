<?php

class MyDBHotline extends SQLite3
{
    function __construct()
    {
        $this->open("bdd/hotlineInfo.sqlite");
    }
}

class HotlineInfo
{   
    private $_db;
    public function __construct(){
        $this->_db=new MyDBHotline();
       /* if ($this->_db) {
         $this->_db->query("CREATE TABLE hotlineur
            (
                idhotlineur INTEGER PRIMARY KEY AUTOINCREMENT,
                nom varchar,
                prenom varchar,
                mail varchar,
                telephone varchar,
                actif bit
            )");        
        } else {
        }
        */
        
    }

    public function newHotlineur($nom,$prenom,$mail,$telephone,$actif){
        $statement =$this->_db
            ->prepare("insert into hotlineur 
            (nom,prenom,mail,telephone,actif)
            values
            (:nom,:prenom,:mail,:telephone,:actif)
            ");
        $statement->bindValue(':nom',$nom);
        $statement->bindValue(':prenom',$prenom);
        $statement->bindValue(':mail',$mail);
        $statement->bindValue(':telephone',$telephone);
        $statement->bindValue(':actif',$actif);
        $statement->execute();
        return $this->listeHotlineur();
    }

    public function listeHotlineur()
    {
        $statement =$this->_db->prepare("SELECT idhotlineur,nom,prenom,mail,telephone,actif
        FROM hotlineur order by actif asc,nom,prenom");
        $result = $statement->execute();
        $icompteur=0;
        while($res = $result->fetchArray(SQLITE3_ASSOC)){ 
            $row[$icompteur]=$res;
            $icompteur++;
         } 
         if(isset($row))
         {
            return $row;
         }
         else {
             return null;
         }
        
    }
    public function updateHotlineur($idhotlineur,$nom,$prenom,$mail,$telephone,$actif)
    {
        $statement =$this->_db
            ->prepare("update hotlineur set 
            nom=:nom,
            prenom=:prenom,
            mail=:mail,
            telephone=:telephone,
            actif=:actif
            where
            idhotlineur=:idhotlineur
            ");
        $statement->bindValue(':idhotlineur',$idhotlineur);
        
        $statement->bindValue(':nom',$nom);
        $statement->bindValue(':prenom',$prenom);
        $statement->bindValue(':mail',$mail);
        $statement->bindValue(':telephone',$telephone);
        $statement->bindValue(':actif',$actif);

        $statement->execute();
        return $this->listeHotlineur();
    }

    public function deleteHotlineur($idhotlineur)
    {
        $statement =$this->_db
            ->prepare("delete from  hotlineur 
            where
            idhotlineur=:idhotlineur
            ");
        $statement->bindValue(':idhotlineur',$idhotlineur);

        $statement->execute();
        return $this->listeHotlineur();
    }

    public function listeTelephoneMailActif()
    {
        $statement =$this->_db->prepare("SELECT telephone,mail
        FROM hotlineur where actif=1");
        $result = $statement->execute();
        $icompteur=0;
        while($res = $result->fetchArray(SQLITE3_ASSOC)){ 
            $row[$icompteur]=$res;
            $icompteur++;
         } 
         return $row;
    }

}
