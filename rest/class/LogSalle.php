<?php

class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open("bdd/Log.sqlite");
    }
}

class LogSalle
{
    private $_idConsultation=0;
    private $_db;
    public function __construct(){
        $this->_db=new MyDB();
        if ($this->_db) {
         /*   $this->_db->query("CREATE TABLE Consultation 
            (
                idconsultation INTEGER PRIMARY KEY AUTOINCREMENT,
                idprescrip INTEGER,
                libelle varchar,
                rpps varchar,
                mail varchar,
                telephone varchar,
                medecin varchar,
                patient varchar,
                consentement bool,
                finess varchar,
                etablissement varchar,
                urlWebMedecin varchar,
                accompagnant varchar,
                n_dossier integer,
                url varchar,
                dateJour varchar,
                idUtilisateur integer,
                telEtablissement varchar,
                idTeleconsultationEnCours int
            )");
            $this->_db->query("CREATE TABLE log (idconsultation INTEGER,creationTime timestamp, statut varchar);");
        */
        } else {
        }
    }

    private function findSalle($rpps,$n_dossier,$finess)
    {

        $statement =$this->_db->prepare("SELECT idconsultation FROM Consultation WHERE rpps = :rpps and n_dossier = :n_dossier and finess = :finess and dateJour=:dateJour");
        $statement->bindValue(':rpps',$rpps);
        $statement->bindValue(':n_dossier',$n_dossier);
        $statement->bindValue(':finess',$finess);
        $statement->bindValue(':dateJour',date('d.m.y'));
        
        $result = $statement->execute();
		if ($row = $result->fetchArray()) {
            $this->_idConsultation=(int)$row[0];
		}
    }
    private function newSalle(Salle $salle){
        $statement =$this->_db
            ->prepare("insert into consultation 
            (idprescrip,libelle,rpps,mail,telephone,medecin,patient,consentement,finess,etablissement,urlWebMedecin,accompagnant,n_dossier,url,dateJour,idUtilisateur,telEtablissement,idTeleconsultationEnCours)
            values
            (:idprescrip,:libelle,:rpps,:mail,:telephone,:medecin,:patient,:consentement,:finess,:etablissement,:urlWebMedecin,:accompagnant,:n_dossier,:url,:dateJour,:idUtilisateur,:telEtab,:idTeleconsultationEnCours)
            ");
        $statement->bindValue(':idprescrip',$salle->getidprescrip());
        $statement->bindValue(':libelle',$salle->getlibelle());
        $statement->bindValue(':rpps',$salle->getrppss());
        $statement->bindValue(':mail',$salle->getmail());
        $statement->bindValue(':telephone',$salle->gettelephone());
        $statement->bindValue(':medecin',$salle->getmedecin());
        $statement->bindValue(':patient',$salle->getpatient());
        $statement->bindValue(':consentement',$salle->getconsentement());
        $statement->bindValue(':finess',$salle->getfiness());
        $statement->bindValue(':etablissement',$salle->getetablissement());
        $statement->bindValue(':urlWebMedecin',$salle->geturlWebMedecin());
        $statement->bindValue(':accompagnant',$salle->getaccompagnant());
        $statement->bindValue(':n_dossier',$salle->getn_dossier());
        $statement->bindValue(':url',$salle->geturl());
        $statement->bindValue(':dateJour',date('d.m.y'));
        $statement->bindValue(':idUtilisateur',$salle->getIdUtilisateur());
        $statement->bindValue(':telEtab',$salle->gettelEtablissement());
        $statement->bindValue(':idTeleconsultationEnCours',$salle->getidteleconsultationEnCours());

        $statement->execute();
        $this->findSalle($salle->getrppss(),$salle->getn_dossier(),$salle->getfiness());
    }


    public function createSalle(Salle $salle){
        $this->findSalle($salle->getrppss(),$salle->getn_dossier(),$salle->getfiness());
        if($this->_idConsultation==0){
            $this->newSalle($salle);
        }
        else {
            $this->updateSalle($salle);
        }
        

        if($this->_idConsultation!=0){
            $statement =$this->_db
            ->prepare("
                insert into log (idconsultation,creationTime,statut)
                values
                (:idconsultation,:creationTime,:statut)
            ");
            $statement->bindValue(':idconsultation',$this->_idConsultation);
            $statement->bindValue(':creationTime',time());
            $statement->bindValue(':statut',"ARRIVÉE DU PATIENT");
            $statement->execute();
        }
    }

    public function debutConsultation($rpps,$n_dossier,$finess,$idteleconsultationencours)
    {
        $this->findSalle($rpps,$n_dossier,$finess);
        if($this->_idConsultation!=0){
            $statementUpdate =$this->_db
            ->prepare("
                update Consultation set 
                idTeleconsultationEnCours=:idTeleconsultationEnCours
                where idconsultation=:idconsultation
            ");
            $statementUpdate->bindValue(':idTeleconsultationEnCours',$idteleconsultationencours);
            $statementUpdate->bindValue(':idconsultation',$this->_idConsultation);
            $statementUpdate->execute();

            $statement =$this->_db
            ->prepare("
                insert into log (idconsultation,creationTime,statut)
                values
                (:idconsultation,:creationTime,:statut)
            ");
            $statement->bindValue(':idconsultation',$this->_idConsultation);
            $statement->bindValue(':creationTime',time());
            $statement->bindValue(':statut',"DÉBUT DE LA CONSULTATION");
            $statement->execute();
        }
    }

    public function finConsultation($rpps,$n_dossier,$finess)
    {
        $this->findSalle($rpps,$n_dossier,$finess);
        if($this->_idConsultation!=0){
            $statement =$this->_db
            ->prepare("
                insert into log (idconsultation,creationTime,statut)
                values
                (:idconsultation,:creationTime,:statut)
            ");
            $statement->bindValue(':idconsultation',$this->_idConsultation);
            $statement->bindValue(':creationTime',time());
            $statement->bindValue(':statut',"FIN DE LA CONSULTATION");
            $statement->execute();
        }
    }
    public function infoSalle($rpps,$n_dossier,$finess)
    {
        $statement =$this->_db->prepare("SELECT idconsultation,idprescrip,libelle,rpps,mail,telephone,
        medecin,patient,consentement,finess,etablissement,urlWebMedecin,accompagnant,n_dossier,url,dateJour,idUtilisateur,telEtablissement,idTeleconsultationEnCours
        FROM Consultation WHERE rpps = :rpps and n_dossier = :n_dossier and finess = :finess and dateJour=:dateJour");
        $statement->bindValue(':rpps',$rpps);
        $statement->bindValue(':n_dossier',$n_dossier);
        $statement->bindValue(':finess',$finess);
        $statement->bindValue(':dateJour',date('d.m.y'));
        
        $result = $statement->execute();
		if ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            return $row;
        }
        else {
           die();
        }
    }

    public function logsSalle($rpps,$n_dossier,$finess)
    {
        $this->findSalle($rpps,$n_dossier,$finess);
        $this->logSalleWithId($this->_idConsultation);
    }

    public function logSalleWithId($idconsultation)
    {
        

        if($idconsultation!=0){
            $statement =$this->_db->prepare("SELECT idconsultation,creationTime,statut FROM log WHERE idconsultation=:idconsultation order by creationTime desc");
            $statement->bindValue(':idconsultation',$idconsultation);
            $result = $statement->execute();
            $icompteur=0;
            while($res = $result->fetchArray(SQLITE3_ASSOC)){ 
                $row[$icompteur]=$res;
                $icompteur++;
             } 
             return $row;
        }
        else
        {
            die("pas trouvé");
        }
    }
    public function listeSalle($date)
    {
        $statement =$this->_db->prepare("SELECT idconsultation,idprescrip,libelle,rpps,mail,telephone,
        medecin,patient,consentement,finess,etablissement,urlWebMedecin,accompagnant,n_dossier,url,dateJour,idUtilisateur,telEtablissement
        FROM Consultation WHERE dateJour=:dateJour order by dateJour");
        $statement->bindValue(':dateJour',$date);
        $result = $statement->execute();
        $icompteur=0;
        while($res = $result->fetchArray(SQLITE3_ASSOC)){ 
            $row[$icompteur]=$res;
            $icompteur++;
         } 
         return $row;
    }
    public function listeSalleAll()
    {
        $statement =$this->_db->prepare("SELECT idconsultation,idprescrip,libelle,rpps,mail,telephone,
        medecin,patient,consentement,finess,etablissement,urlWebMedecin,accompagnant,n_dossier,url,dateJour,idUtilisateur,telEtablissement
        FROM Consultation order by dateJour desc,idconsultation desc ");
        $result = $statement->execute();
        $icompteur=0;
        while($res = $result->fetchArray(SQLITE3_ASSOC)){ 
            $row[$icompteur]=$res;
            $icompteur++;
         } 
         return $row;
    }
    public function updateSalle($salle)
    {
        $statement =$this->_db
            ->prepare("update Consultation set 
            idprescrip=:idprescrip,
            libelle=:libelle,
            rpps=:rpps,
            mail=:mail,
            telephone=:telephone,
            medecin=:medecin,
            patient=:patient,
            consentement=:consentement,
            finess=:finess,
            etablissement=:etablissement,
            urlWebMedecin=:urlWebMedecin,
            accompagnant=:accompagnant,
            n_dossier=:n_dossier,
            url=:url,
            dateJour=:dateJour,
            idUtilisateur=:idUtilisateur,
            telEtablissement=:telEtab,
            idTeleconsultationEnCours=:idTeleconsultationEnCours
            where
            idconsultation=:idconsultation
            ");
        $statement->bindValue(':idprescrip',$salle->getidprescrip());
        $statement->bindValue(':libelle',$salle->getlibelle());
        $statement->bindValue(':rpps',$salle->getrppss());
        $statement->bindValue(':mail',$salle->getmail());
        $statement->bindValue(':telephone',$salle->gettelephone());
        $statement->bindValue(':medecin',$salle->getmedecin());
        $statement->bindValue(':patient',$salle->getpatient());
        $statement->bindValue(':consentement',$salle->getconsentement());
        $statement->bindValue(':finess',$salle->getfiness());
        $statement->bindValue(':etablissement',$salle->getetablissement());
        $statement->bindValue(':urlWebMedecin',$salle->geturlWebMedecin());
        $statement->bindValue(':accompagnant',$salle->getaccompagnant());
        $statement->bindValue(':n_dossier',$salle->getn_dossier());
        $statement->bindValue(':url',$salle->geturl());
        $statement->bindValue(':dateJour',date('d.m.y'));
        $statement->bindValue(':idUtilisateur',$salle->getIdUtilisateur());
        $statement->bindValue(':telEtab',$salle->gettelEtablissement());
        $statement->bindValue(':idTeleconsultationEnCours',$salle->getidteleconsultationEnCours());
        $statement->bindValue(':idconsultation',$this->_idConsultation); 

        $statement->execute();
    }

}
