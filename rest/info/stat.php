<?php
 $AUTHORIZED_IP = array("82.240.45.23","109.190.94.78","37.58.176.54","92.154.29.239","::1");
 if(in_array($_SERVER['REMOTE_ADDR'],$AUTHORIZED_IP)==false)
 {
     die;
 }
?>

<script>
    var tab 
    window.onload= function(e){
        <?php
        require("controllerSalle.php");
        $controllerSalle=new controllerSalle();
        $request=$controllerSalle->getListeSalle();
        $reel=true;
        ?>
        tab =<?php echo json_encode($request);?>;        
        }

        function strpos (haystack, needle, offset) {
            var i = (haystack+'').indexOf(needle, (offset || 0));
            return i === -1 ? false : i;
        }

        function loadTable()
        {
            var spanContent=document.getElementById("tableContent")
            spanContent.innerHTML=""
            var tabOk =[]
            var tabCount=[]
            var tabNomEtab=[]
            var dateDebut= new Date(document.getElementById("start").value)
            var dateEnd=new Date(document.getElementById("end").value)
            tab.forEach(function(element){
                var dateParse="20"+element["dateJour"].substring(6,8)
                dateParse=dateParse+'/'+element["dateJour"].substring(3,5)
                dateParse=dateParse+'/'+element["dateJour"].substring(0,2)
                var dateElement=new Date(dateParse);
                if ( dateElement>=dateDebut && dateElement<=dateEnd)
                {
                   if(element["mail"]!="" && strpos(element["mail"],"malta-informat")==false && element["mail"]!="v.klotz@groupecolisee.com" ){
                        tabOk.push(element)
                }
                }
            });
            tabOk.forEach(function(element){
                var stringFiness=element["finess"]
                if(typeof tabCount[stringFiness] =="undefined")
                {
                    tabCount[stringFiness]=1;
                    tabNomEtab[stringFiness]=element["etablissement"]
                }
                else
                {
                    tabCount[stringFiness]++
                }
                
            });
            

            for (var key in tabCount) {
                var tr = document.createElement("tr")
                var tdEtab = document.createElement("td")
                var tdFiness = document.createElement("td")
                var tdCount = document.createElement("td")
                tdEtab.innerHTML=tabNomEtab[key]
                tdFiness.innerHTML=key
                tdCount.innerHTML=tabCount[key]
            
                tr.appendChild(tdEtab)
                tr.appendChild(tdFiness)
                tr.appendChild(tdCount)
                spanContent.appendChild(tr)
            }

        }
</script>
<html>
    <head>
        <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            /*width: 100%;*/
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        </style>
    </head>
    <body >
    <div>
        <label for="start">Start date:</label>
        <input type="date" id="start" name="trip-start"
        value=<?php echo date('Y-m-d'); ?>
        />
        <label for="end">End date:</label>
        <input type="date" id="end" name="trip-start"
        value=<?php echo date('Y-m-d'); ?>
        />
        <button onclick="loadTable()">Search</button>
    </div>
    <div>
    <table>
        <thead>
        <tr>
            <th>Etablissement</th>
            <th>Finess</th>
            <th>Nombre</th>
        </tr>
        </thead>
        <tbody  id="tableContent">

        </tbody>
    </table>
    </div>
</body>
    
    </html>
