<?php
 $AUTHORIZED_IP = array("82.240.45.23","109.190.94.78","37.58.176.54","92.154.29.239","::1");
 if(in_array($_SERVER['REMOTE_ADDR'],$AUTHORIZED_IP)==false)
 {
     die;
 }
?>
<html>
    <head>
        <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            /*width: 100%;*/
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        </style>
    </head>
    <body>

        <?php
        require("controllerSalle.php");
        $controllerSalle=new controllerSalle();
        if(isset($_GET["date"]))
        {
            $date=$_GET["date"];
            $date=str_replace('/','.',$date);
            $request=$controllerSalle->getListeSalle($date);
        }
        else {
            $request=$controllerSalle->getListeSalle();
        }
        $reel=false;
        if(isset($_GET["reel"]))
        {
          $reel=true;  
        }
    
        ?>
            <table>
                <tr>
                    <th>Date</th>
                    <th>id</th>
                    <th>Etablissement</th>
                    <th>Finess</th>
                    <th>Medecin</th>
                    <th>Idprescrip</th>
                    <th>Rpps</th>
                    <th>Mail</th>
                    <th>Telephone</th>
                    <th>Patient</th>
                    <th>n_dossier</th>
                    <th>consementement</th>
                    <th>accompagnant</th>
                    <th>idutilisateur</th>
                    <th>url</th>
                </tr>
                <?php
            if(sizeof($request)>0)
            {
            foreach ($request as $row){
                if($reel==false || ($row["mail"]!="" && strpos($row["mail"],"malta-informat")==false && $row["mail"]!="v.klotz@groupecolisee.com" ))
                {
            ?>
            <tr>
                <td><?php echo(str_replace('.','/',$row["dateJour"])) ?></td>
                <td><?php echo($row["idconsultation"]) ?></td>
                <td><?php echo($row["etablissement"]) ?></td>
                <td><?php echo($row["finess"]) ?></td>
                <td><?php echo($row["medecin"]) ?></td>
                <td><?php echo($row["idprescrip"]) ?></td>
                <td><?php echo($row["rpps"]) ?></td>
                <td><?php echo($row["mail"]) ?></td>
                <td><?php echo($row["telephone"]) ?></td>
                <td><?php echo($row["patient"]) ?></td>
                <td><?php echo($row["n_dossier"]) ?></td>
                <td><?php echo($row["consentement"]) ?></td>
                <td><?php echo($row["accompagnant"]) ?></td>
                <td><?php echo($row["idUtilisateur"]) ?></td>
                <td><?php echo($row["url"]) ?></td>
                <td>
                <button onclick="window.location.href='./infoSalle.php?idconsultation=<?php echo($row['idconsultation']) ?> '">LOG</button>
                

            </tr>
            <?php
                }
            }
            }
            ?>

            </table>
    </body>
</html>