<?php
 $AUTHORIZED_IP = array("82.240.45.23","109.190.94.78","37.58.176.54","92.154.29.239","::1");
 if(in_array($_SERVER['REMOTE_ADDR'],$AUTHORIZED_IP)==false)
 {
     die;
 }
?>
<html>
    <head>
        <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            /*width: 100%;*/
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        </style>
    </head>
    <body>
        <?php
        require("controllerSalle.php");
        $request=null;
        if(isset($_GET["idconsultation"]))
        {
            $controllerSalle=new controllerSalle();
            $request=$controllerSalle->getLogSalle($_GET["idconsultation"]);
        }
        ?>
        <table>
        <tr>
            <th>Date heure</th>
            <th>statut</th>
        </tr>
        <?php
        if(sizeof($request)>0)
        {
            foreach ($request as $row){
                ?>
                <tr>
                <td><?php echo(date("d/m/y\ H:i:s\ ",$row["creationTime"])) ?></td>
                <td><?php echo($row["statut"]) ?></td>
                </tr>
                <?php
            }
        }
        ?>

        </table>
        <button onclick="window.location.href='./listeSalle.php'">Retour</button>
    </body>
</html>




