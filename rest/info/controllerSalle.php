<?php
class controllerSalle
{
    //private $_webservice="http://192.168.1.74/titanwebrtc/rest/wsvisio.php";
    private $_webservice="http://update.malta-informatique.fr/WS/Visio/wsvisio.php";

    function __construct()
    {
    }

    public function getListeSalle($date=null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_webservice."?listeSalle");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        if($date!=null)
        {
            $array[0]["Date"]=$date;
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($array, JSON_UNESCAPED_UNICODE));
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain'));
        
        $result = curl_exec($ch);
        curl_close($ch);
        return (json_decode($result,true));
    }
    public function getLogSalle($idconsultation)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_webservice."?logConsultation");
        $array[0]["idconsultation"]=$idconsultation;
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($array, JSON_UNESCAPED_UNICODE));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain'));
        
        $result = curl_exec($ch);
        curl_close($ch);
        return (json_decode($result,true));
    }
}



