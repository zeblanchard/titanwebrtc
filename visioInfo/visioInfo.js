var tabn_dossierDansSalle = [] ;
$(function () {
    'use strict';

    apiRTC.setLogLevel(10);


    var connectedConversation = null,
        connectedSession = null;
    /**
     * Create and join a new conference
     * @author Apizee
     *
     * @param {string} name - Conference name
     * @return void
     */
    function joinConference(name) {
//TODO: Creer un compte sur plateforme HDS
        var cloudUrl = 'https://cloud.apizee.com';
        var contact = null;
        var ua = null;

        //==============================
        // CREATE USER AGENT
        //==============================
        ua = new apiRTC.UserAgent({
//TODO: Creer un compte sur plateforme HDS
            uri: 'apzkey:myDemoApiKey'
        });
        //==============================
        // REGISTER
        //==============================

        ua.register({
            cloudUrl: cloudUrl,
            })
                .then(function (session) {
                    
                    // Save session
                    connectedSession = session;
                    connectedSession.joinGroup(name);
                    connectedSession.leaveGroup("default");
                    connectedSession
                        .on("contactListUpdate", function (updatedContacts) { //display a list of connected users
                            if (typeof updatedContacts.joinedGroup[name] != 'undefined')
                            {    
                                for (var i = 0; i < updatedContacts.joinedGroup[name].length; i += 1) {
                                    if(updatedContacts.joinedGroup[name][i].userData.username.indexOf("|")!=-1)
                                    {
                                        tabn_dossierDansSalle.push(updatedContacts.joinedGroup[name][i].userData.username.split("|")[0]);
                                    }
                                }
                            }                         
                            if (typeof updatedContacts.leftGroup[name] != 'undefined')
                            {   
                                for (var i = 0; i < updatedContacts.leftGroup[name].length; i += 1) {
                                    if(updatedContacts.leftGroup[name][i].userData.username.indexOf("|")!=-1)
                                    {
                                        var index = tabn_dossierDansSalle.indexOf(updatedContacts.leftGroup[name][i].userData.username.split("|")[0]);
                                        if (index > -1) {
                                            tabn_dossierDansSalle.splice(index, 1);
                                        }
                                    }
                                }
                            }
                            console.error(tabn_dossierDansSalle);
                            var divnb=document.getElementById("result")
                            divnb.innerHTML=tabn_dossierDansSalle;

                        });
            }
        );
    }
    
    function abonnementSalle(nom)
    {
        joinConference(nom);
    }
    $("#inputSaisieTexteValider").click(function(e){
        e.preventDefault();
        abonnementSalle($("#inputSaisieTexte").val());
    });
});