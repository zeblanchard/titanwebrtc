<?php
    if (isset($_GET["room"])) {
		$room = $_GET["room"];
		$room=urlencode($room);
	    if (isset($_GET["mode"])) {
			$mode= $_GET["mode"];
			if($mode=="medecin"){
				header("Location: html/medecin.html?room=$room&mode=medecin");
				exit();
			}
			else {
				header("Location: html/patient.html?room=$room");
				exit();
			}
		}
	
	}
?>