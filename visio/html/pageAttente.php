<html lang="fr">
	<head>
			<title>Salle attente</title>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="icon" type="image/png" href="./img/favicon.png" />

			<!-- Styles -->
			<link href="../css/pageAttente/pageAttente.css" rel="stylesheet">
	</head>
	<body>
        <div class="loader">
		<?php
		if(isset($_GET["leave"]))
		{
		?>
				<div class="textLoader">Le médecin a quitté la salle sans mettre fin à la téléconsultation,<br/>
								il a peut-être subi une coupure de connexion.
				                <?php if(isset($_GET["tel"]) && $_GET["tel"]!=""){?><br/>Vous pouvez le joindre au <?php echo($_GET["tel"]); }?></div>
			<div class="loaderLeave"></div>
		<?php
		}
		else {
			if(isset($_GET["kick"]))
			{
				?>
				<div class="textLoader">La téléconsultation a été clôturée par le médecin.</div>
				<!-- <div class="loaderKick"></div> -->
				<?php
			}
			else {
			?>
				<div class="textLoader">Le médecin a été prévenu de votre arrivée, merci de patienter.<?php if(isset($_GET["tel"]) && $_GET["tel"]!="" ){?><br/>Vous pouvez le joindre au <?php echo($_GET["tel"]);} ?></div>
				<div class="loaderImg"></div>
			<?php
			}
		}
		?>

        </div>
    </body>

</html>