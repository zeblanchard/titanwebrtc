var 
versionScript= "1.4"
localCallid = null,
traceActive = 0,
MODE_MEDECIN="medecin",
TYPE_LOCAL="local",
connectedSession = null,
TYPE_REMOTE="remote";
function urldecode(url) {
    if (typeof url != 'undefined')
    {
     return decodeURIComponent(url.replace(/\+/g, ' '));
    }
    else
    {
        return "";
    }
  }

$(function () {
    'use strict';
    console.warn("VERSION SCRIPT :",versionScript);
    apiRTC.setLogLevel(1);
    
    function displayUAInfos() {
        console.info("browser :", apiRTC.browser);
        console.info("browser_major_version :", apiRTC.browser_major_version);
        console.info("browser_version :", apiRTC.browser_version);
        console.info("osName :", apiRTC.osName);
    }
    displayUAInfos();
    //Query parameter management
    var urlParams = function () {
        var queryString = {},
            query = window.location.search.substring(1),
            vars = query.split("&"),
            i = 0;

        for (i = 0; i < vars.length; i += 1) {

            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof queryString[pair[0]] === "undefined") {
                queryString[pair[0]] = pair[1];
                // If second entry with this name
            } else if (typeof queryString[pair[0]] === "string") {
                var arr = [queryString[pair[0]], pair[1]];
                queryString[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                queryString[pair[0]].push(pair[1]);
            }
        }
        if(typeof queryString.mode === 'undefined')
        {
            queryString.mode="patient";
        }
        if(typeof queryString.username === 'undefined')
        {
            username= queryString.mode;
        }
        if(typeof queryString.trace === 'undefined')
        {
            queryString.traceActive= 0;
        }

        return queryString;
    }();
    console.log("urlParams", urlParams);
    traceActive = urlParams.trace;
    var conferenceName = urlParams.room,
        username = urldecode(urlParams.username),
        mode = urlParams.mode,
        localStream = null,
        ua = null,
        whiteBoardClient = null,
        connectedConversation = null,
        bkgPhoto = null,
        numeroImage =1,
        xEncours = null,
        yEncours = null,
        FontColor="#000000",
        nbContact=0,
        tabContact={},
        subscribedStreams = {},
        localidCamera=0,
        remoteIdCamera=0,
        numWhiteBoard,
        localidMicro=0,
        telMedecin=urldecode(urlParams.telMedecin),
        qosStats= {};

    if(mode!=MODE_MEDECIN)
    {
        $("#modal-attente").attr('src',"../html/pageAttente.php?tel="+telMedecin)
        $('.control').css("display","none"); 

    }
    
    
    function replaceStream(id,type,id2=0)
    {
        console.log("replaceStream");
        var option = ""
        switch(type)
        {
            case "all" :
                option={videoInputId: id ,audioInputId : id2,enhancedAudioActivated : true};
                break;
            case "audio" :
                if(localidCamera!=0)
                {
                    option={audioInputId: id ,videoInputId :localidCamera ,enhancedAudioActivated : true};
                }
                else
                {
                    option={audioInputId: id ,enhancedAudioActivated : true};  
                }
                localidMicro=id
                break;
            case "camera" :
                if(localidMicro!=0)
                    {
                        option={videoInputId: id , audioInputId: localidMicro,enhancedAudioActivated : true};
                    }
                    else
                    {
                        option={videoInputId: id,enhancedAudioActivated : true};
                    }
                    localidCamera=id
                    break;
            case "none" :
                if(localidMicro!=0)
                {
                    if(localidCamera!=0)
                    {
                        option={videoInputId: localidCamera , audioInputId: localidMicro,enhancedAudioActivated : true};
                    }
                    else
                    {
                        option={audioInputId: localidMicro,enhancedAudioActivated : true};
                    }
                }
                else
                {
                    if(localidCamera!=0)
                    {
                        option={videoInputId: localidCamera,enhancedAudioActivated : true};
                    }
                    else
                    {
                        option={enhancedAudioActivated : true};
                    }
                }
              
        }

        let callbacks = {};
        var call = connectedConversation.getConversationCall(localStream);
        console.log('call :', call);
        callbacks.getStream = () => {
            return new Promise((resolve, reject) => {
            console.log('getStream');
            localStream.release();
            ua.createStream(option)
                .then((newstream) => 
                { 
                    console.log('createStream for replaceStream OK !! ', newstream);
                    addLocalStream(newstream, call.callId);
                    return resolve(newstream);
                } 
                )
                .catch(reject);
                });
        };
        call.replacePublishedStream(null, callbacks);
}

    function updateDeviceList(res,userId){
        console.log('updateDeviceList',res);

        var cameras = [],
            microphones = [],
            option = null,
            i = 0,
            v = 0,
            contact=connectedSession.getContact(userId),
            compteur =0,
            //TODO: methode temporaire tant que je n'arrive pas à récuperer le callId propement 
            callid=SenderIdToCallID(contact.getId());

        if($("#remote-media-"+callid).length>0)
        {   
            $("#remote-media-controlRight-"+callid).remove();
            var streamSelecteur=document.getElementById("remote-media-" + callid);
            var ControlRight= document.createElement("div");
            ControlRight.className="controlRight";
            ControlRight.id="remote-media-controlRight-"+callid;
            streamSelecteur.appendChild(ControlRight);

            console.log('fetchUserMediaDevices', (res));
            for (i = 0; i < Object.values(res.videoinput).length; i++) {
                var spanCamera=document.createElement("span");
                spanCamera.className="icon-currentVideo";
                compteur++
                v = Object.values(res.videoinput)[i];
                console.log('getCameras', v);
                option = document.createElement("div");
                option.innerText=compteur;
                option.className="currentVideo";
                option.title=v.label || 'camera ' + (compteur);
                option.id="remote-media-currentVideo-"+callid+"-"+compteur;
                option.setAttribute("idCamera",v.id);
                option.setAttribute("callId",callid);
                if(remoteIdCamera==0){
                    if(compteur===1){
                        option.className="currentVideo cameraSelected";
                        remoteIdCamera=v.id
                    }
                    else{
                        option.className="currentVideo";
                    }
                }
                else
                {
                    if(remoteIdCamera==v.id)
                    {
                        option.className="currentVideo cameraSelected";  
                    }
                    else
                    {
                        option.className="currentVideo";  
                    }
                }
         
                ControlRight.appendChild(option);
                //TODO: pouvoir savoir si la camera est celle utilisé dans le stream
                option.appendChild(spanCamera);
          
            $("#remote-media-currentVideo-"+callid+"-"+compteur).click(function(e){
                var element = document.getElementById(this.id);
                remoteIdCamera=$("#"+this.id).attr("idCamera")
                $('#'+element.parentNode.id).children().removeClass('cameraSelected');

                element.className="currentVideo cameraSelected";
                contact.sendData({
                    order: 'switchCamera',
                    id:  $("#"+this.id).attr("idCamera")
                });
            });
            }

            //code temporaire pour test 
            /*
            var compteurAudio=0
            for (i = 0; i < Object.values(res.audioinput).length; i++) {
                var spanAudio=document.createElement("span");
                spanAudio.className="icon-currentAudio";
                v = Object.values(res.audioinput)[i];
                if(v.id!="default" && v.id!="communications")
                {   
                    compteurAudio++
                    console.log('getAudio', v);
                    option = document.createElement("div");
                    option.innerText="1"+compteurAudio;
                    option.className="currentAudio";
                    option.title=v.label || 'audio ' + (compteurAudio);
                    option.id="remote-media-currentAudio-"+callid+"-"+compteurAudio;
                    option.setAttribute("idAudio",v.id);
                    option.setAttribute("callId",callid);

                    if(localidMicro==0){
                        if(compteurAudio===1){
                            option.className="currentAudio audioSelected";
                        }
                        else{
                            option.className="currentAudio";
                        }
                    }
                    else
                    {
                        if(localidMicro==v.id)
                        {
                            option.className="currentAudio audioSelected";  
                        }
                        else
                        {
                            option.className="currentAudio";  
                        }
                    }
                    ControlRight.appendChild(option);
                    //TODO: pouvoir savoir si la camera est celle utilisé dans le stream
                    option.appendChild(spanAudio);
            
                    $("#remote-media-currentAudio-"+callid+"-"+compteurAudio).click(function(e){
                        var element = document.getElementById(this.id);
                        $('#'+element.parentNode.id).children().removeClass('audioSelected');

                        element.className="currentAudio audioSelected";
                        contact.sendData({
                            order: 'switchAudio',
                            id:  $("#"+this.id).attr("idAudio")
                        });
                    });
                }
            }
            */




            if($("#remote-media-control-"+callid).is(":visible"))
            {   
                $("#remote-media-controlRight-"+callid). fadeIn(300)     
            }
        }      
    }
    
    function SenderIdToCallID(Senderid){
        var activeSteam=connectedConversation.getAvailableStreamList();
        var callId=null;
        for (var i = 0;i<activeSteam.length; i++) {
            if($("#remote-media-"+activeSteam[i].callId).length>0 && activeSteam[i].contact.getId()==Senderid )
            {
                callId=activeSteam[i].callId;
            }
        }

        return callId;
    }


    function addLeaveButton(stream,callId,type){
        var div=document.getElementById(type+"-media-caption-" + callId)
        var quitter=document.createElement("div"),
        imgQuitter=document.createElement("span");

        quitter.className="close btn"
        quitter.id=type+"-media-quitter" + callId;
        quitter.title="Dépublier";
        div.appendChild(quitter);

        imgQuitter.className="icon-close";
        quitter.appendChild(imgQuitter);

        $('#'+type+"-media-quitter" + callId).click(function () {
            stream.getContact().sendData({
                order: 'kick'
            });

           /* connectedConversation.unpublish(stream);    
            stream.release();
            $('#'+type+"-media-" + callId).remove();
            $("#cameraListe_"+stream.getContact().getId()).remove();
            */
        });
    }

    function addInfoButton(stream,callId,type){
        var div=document.getElementById(type+"-media-caption-" + callId)
        var info=document.createElement("div"),
        imgInfo=document.createElement("span");

        info.className="info btn"
        info.id=type+"-media-info" + callId;
        info.title="Info";
        div.appendChild(info);

        imgInfo.className="icon-info";
        info.appendChild(imgInfo);

        $('#'+type+"-media-info" + callId).click(function () {
            var modal = document.getElementById('modal-info-'+type);
            modal.style.display = "block";
            modal.onclick = function() {
                modal.style.display = "none";
              }

        });
    }

    function addStreamCaption(stream, callId,type) {
        var conteneur = document.getElementById(type+"-media-" + callId),
            div=document.createElement("div");
            div.className="caption video";
            div.id=type+"-media-caption-" + callId;

            if(type==TYPE_LOCAL)
            {
                div.innerText="Moi";
            }
            else
            {   
                var contact=stream.getContact();
                var usernameRemote=contact.getUsername();
                if(usernameRemote.indexOf("|")!=-1)
                {
                    div.innerText=usernameRemote.split("|")[1];
                }
                else
                {
                    div.innerText=usernameRemote;
                }
            }
            conteneur.appendChild(div);
            if(type==TYPE_REMOTE && mode==MODE_MEDECIN){
                addLeaveButton(stream, callId,type);
            }
            addInfoButton(stream, callId,type)
    }



    function addParameterLocal(callId,type,stream)
    {
        if(type==TYPE_LOCAL)
        {
            var divbtmControl =document.getElementById(type+"-media-btmControl" + callId);
            var divparameter = document.createElement("div");
            divparameter.className="btnPlay btn";
            divparameter.title="Paramètre";
            divparameter.id=type+"-media-btmControl-parameter-" + callId
            divbtmControl.appendChild(divparameter);
            var spanParameter = document.createElement("span");
            spanParameter.className="icon-parameter";
            divparameter.appendChild(spanParameter);

            $("#"+type+"-media-btmControl-parameter-" + callId).on('click', function(e) {
                var divTeleconsulte=document.getElementById("teleconsultation")
                $(".modal-setting").remove();
                var divModalSetting=document.createElement("div");
                divModalSetting.className="modal-setting";
                xEncours = e.pageX ; 
                if (mode==MODE_MEDECIN){
                    yEncours = e.pageY  ;
                }
                else
                {
                    yEncours = e.pageY  -100;
                }

                divModalSetting.style.display = "block";
                divModalSetting.style.top = yEncours+'px';
                divModalSetting.style.left= xEncours+'px';
                divModalSetting.style.position = 'absolute';

                divTeleconsulte.appendChild(divModalSetting);
                
                var formModalSetting=document.createElement("form");
                formModalSetting.action="";
                formModalSetting.onsubmit="";
                divModalSetting.appendChild(formModalSetting);
                var divSelectCamera=document.createElement("div");
                formModalSetting.appendChild(divSelectCamera);
                var labelSelectCamera=document.createElement("label");
                labelSelectCamera.for="selectLocalMicro";
                labelSelectCamera.innerText="Camera :"
                divSelectCamera.appendChild(labelSelectCamera);
                var SelectCamera=document.createElement("select");
                SelectCamera.id="selectLocalCamera";

                divSelectCamera.appendChild(SelectCamera);
                
                var res=ua.getUserMediaDevices();
                // remplir la liste des camera
                var i=0,
                compteur=0;
                for (i = 0; i < Object.values(res.videoinput).length; i++) {
                    var optionCamera=document.createElement("option");
                    compteur++
                   var  v = Object.values(res.videoinput)[i];

                    //TODO: pouvoir savoir si la camera est celle utilisé dans le stream
                    optionCamera.innerText=v.label;
                    optionCamera.value=v.id;
                    if(localidCamera==0)
                    {
                        if(compteur===1){
                            localidCamera=v.id
                        }
                    }
                    else
                    {
                        if(localidCamera==v.id)
                        {
                            optionCamera.selected="selected";
                        }
                    }
        
                    SelectCamera.appendChild(optionCamera);
                }
  
            var divSelectMicro=document.createElement("div");



            formModalSetting.appendChild(divSelectMicro);
            var labelSelectMicro=document.createElement("label");
            labelSelectMicro.for="selectLocalMicro";
            labelSelectMicro.innerText="Micro :"
            divSelectMicro.appendChild(labelSelectMicro);
            var SelectMicro=document.createElement("select");
            SelectMicro.id="selectLocalMicro";
            divSelectMicro.appendChild(SelectMicro);

            i=0;
            compteur=0;
            for (i = 0; i < Object.values(res.audioinput).length; i++) {
                var optionMicro=document.createElement("option");
                compteur++
               var  v = Object.values(res.audioinput)[i];
                if(v.id!="default" && v.id!="communications")
                {
                    //TODO: pouvoir savoir si la camera est celle utilisé dans le stream
                    optionMicro.innerText=v.label;
                    optionMicro.value=v.id;
                    if(localidMicro==0)
                    {
                        if(compteur===1){
                            localidMicro=v.id
                        }
                    }
                    else
                    {
                        if(localidMicro==v.id)
                        {
                            optionMicro.selected="selected";
                        }
                    }
                    SelectMicro.appendChild(optionMicro);
                }
            }

            var btnValider=document.createElement("button");
            btnValider.innerText="valider";
            btnValider.id="btnValiderParameter"
            formModalSetting.appendChild(btnValider);
            if(mode==MODE_MEDECIN)
            {
                $('.control').fadeOut(100);
                $('.controlRight').fadeOut(100);
            }


   

            
            $("#btnValiderParameter").on('click', function(e) {
                e.preventDefault();
                var optionCamera=document.getElementById("selectLocalCamera");
                localidCamera=optionCamera.value;
                console.log("localidCamera",localidCamera);

                var optionMicro=document.getElementById("selectLocalMicro");
                localidMicro=optionMicro.value;
                console.log("localidMicro",localidMicro);
                replaceStream(localidCamera,"all",localidMicro)

                $(".modal-setting").remove();

            });
        });

        }
    }

    function addActionButton(stream, callId,type){
        var videoContainer = document.getElementById(type+"-media-" + callId);
        var divControl =document.createElement("div");
        divControl.className="control";
        divControl.id=type+"-media-control-" + callId;
        videoContainer.appendChild(divControl);
        var divbtmControl =document.createElement("div");
        divbtmControl.className="btmControl"
        divbtmControl.id=type+"-media-btmControl" + callId;
        divControl.appendChild(divbtmControl);
        if(mode==MODE_MEDECIN)
        {
            if(type==TYPE_LOCAL){
                addMuteVideo(callId,type,stream);
                addMuteMicrophone(callId,type,stream);
                addParameterLocal(callId,type,stream);
            }
            else
            {
                addTakeSnapshot(callId,type,stream);
                addMuteSound(callId,type)
            }
            addfullScreen(callId,type);
        }
        else
        {   
            if(type==TYPE_LOCAL){
                addStetoscope(callId,type,stream);
                addParameterLocal(callId,type,stream);
            }
            if( $("#modal-attente").is(":hidden"))
            {
                $('.control').css("display","block"); 
            }
            else
            {
                $('.control').css("display","none"); 
            }
        }
        

        var video= $('#'+type+"-media-video-"+ callId);
        video[0].removeAttribute("controls");
        if(mode==MODE_MEDECIN)
        {
            video.on('mouseover',function(){
                if($(".modal-setting").length<1)
                {
                    $('.control').fadeOut(100);
                    $('.controlRight').fadeOut(100);
                    $('#'+type+"-media-control-" + callId).fadeIn(300);
                    $('#'+type+"-media-controlRight-" + callId).fadeIn(300);
                }
            });
        }
    }

    function addTakeSnapshot(callId,type,stream)
    {
        var divbtmControl =document.getElementById(type+"-media-btmControl" + callId);
        var divSnapshot = document.createElement("div");
        divSnapshot.className="btnPlay btn snapshotbtn";
        divSnapshot.title="Prendre une photo";
        divSnapshot.id=type+"-media-btmControl-Snapshot-" + callId
        divbtmControl.appendChild(divSnapshot);
        var spanSnapshot = document.createElement("span");
        spanSnapshot.className="icon-Snapshot";
        divSnapshot.appendChild(spanSnapshot);
        if(type==TYPE_LOCAL)
        {
            $("#"+type+"-media-btmControl-Snapshot-" + callId).on('click', function() {
                stream.takeSnapshot()
                .then((media) => {
                    // OK
                    $('#snapshots').append('<a href="' + media + '" target="_blank"><img src="' + media + '" /></a>');
                })
                .catch((error) => {
                    // error
                });
            });

        }
        else
        {
            $("#"+type+"-media-btmControl-Snapshot-" + callId).on('click', function(e) {
                //$(".snapshotbtn").hide();
                if($(".loaderImgSnapshot").length==0)
                {
                    $(".icon-Snapshot").removeClass("icon-Snapshot").addClass("loaderImgSnapshot");
                    e.target
                    stream.getContact().sendData({
                        order: 'takeSnapshot',
                        callId: callId,
                        senderId: connectedSession.getId()
                    });
                }

            });
        }
    }

    function addMuteVideo(callId,type,stream)
    {
        var divbtmControl =document.getElementById(type+"-media-btmControl" + callId);
        var divVideo = document.createElement("div");
        divVideo.className="video btn";
        divVideo.title="Masquer la vidéo";
        divVideo.id=type+"-media-btmControl-MuteVideo-" + callId
        divbtmControl.appendChild(divVideo);
        var spanVideo = document.createElement("span");
        spanVideo.className="icon-video";
        divVideo.appendChild(spanVideo);
        if(stream.isVideoMuted()==true || stream.hasVideo()==false ){
            $("#"+type+"-media-btmControl-MuteVideo-" + callId).toggleClass('muted');
        }
        $("#"+type+"-media-btmControl-MuteVideo-" + callId).on('click', function() {
            var muted=$(this).toggleClass('muted');

            if (stream.isVideoMuted()==false) {
                stream.muteVideo();
            }
            else {
                stream.unmuteVideo();
            }
        });
    }

    function addStetoscope(callId,type,stream)
    {
        var divbtmControl =document.getElementById(type+"-media-btmControl" + callId);
        var divSteto = document.createElement("div");
        divSteto.className="steto btn";
        divSteto.title="Activer le retour audio du stétoscope";
        divSteto.id=type+"-media-btmControl-Stetoscope-" + callId
        divbtmControl.appendChild(divSteto);
        var spanSteto = document.createElement("span");
        spanSteto.className="icon-steto";
        divSteto.appendChild(spanSteto);
        $("#"+type+"-media-btmControl-Stetoscope-" + callId).on('click', function() {
            if ($("#"+type+"-media-btmControl-Stetoscope-" + callId).hasClass("activated")!=true)
            {
                this.title="Désactiver le retour audio du stétoscope"
                var videoLocalStream=document.getElementById(type+"-media-video-"+callId)
                videoLocalStream.muted=false;
                $("#"+type+"-media-btmControl-Stetoscope-" + callId).addClass("activated")
            }
            else
            {
                this.title="Activer le retour audio du stétoscope"
                var videoLocalStream=document.getElementById(type+"-media-video-"+callId)
                videoLocalStream.muted=true;
                $("#"+type+"-media-btmControl-Stetoscope-" + callId).removeClass("activated")
            }
        });
    }


    function addMuteMicrophone(callId,type,stream)
    {
        var divbtmControl =document.getElementById(type+"-media-btmControl" + callId);
        var divMicro = document.createElement("div");
        divMicro.className="sound btn";
        divMicro.title="Désactiver le micro";
        divMicro.id=type+"-media-btmControl-MuteMicrophone-" + callId
        divbtmControl.appendChild(divMicro);
        var spanMicro = document.createElement("span");
        spanMicro.className="icon-sound";
        divMicro.appendChild(spanMicro);
        if(stream.isAudioMuted()==true || stream.hasAudio()==false ){
            $("#"+type+"-media-btmControl-MuteMicrophone-" + callId).toggleClass('muted');
        }
        $("#"+type+"-media-btmControl-MuteMicrophone-" + callId).on('click', function() {
            var muted=$(this).toggleClass('muted');

            if (stream.isAudioMuted()==false) {
                stream.muteAudio();
            }
            else {
                stream.unmuteAudio();
            }
        });


    }

    function addMuteSound(callId,type)
    {
        var divbtmControl =document.getElementById(type+"-media-btmControl" + callId);
        var divSound = document.createElement("div");
        divSound.className="sound btn";
        divSound.title="Désactiver le son";
        divSound.id=type+"-media-btmControl-MuteSound-" + callId
        divbtmControl.appendChild(divSound);
        var spanSound = document.createElement("span");
        spanSound.className="icon-sound";
        divSound.appendChild(spanSound);

        $("#"+type+"-media-btmControl-MuteSound-" + callId).on('click', function() {
            var muted=$(this).toggleClass('muted');
            var video = document.getElementById(type+"-media-video-" + callId)
            video.muted=!video.muted;
        });


    }


    function addfullScreen(callId,type){

        var divbtmControl =document.getElementById(type+"-media-btmControl" + callId);
        var divFS = document.createElement("div");
        divFS.className="btnFS btn";
        divFS.title="Plein écran";
        divFS.id=type+"-media-btmControl-FullScreen-" + callId
        divbtmControl.appendChild(divFS);
        var spanFs = document.createElement("span");
        spanFs.className="icon-fullscreen";
        divFS.appendChild(spanFs);

        $("#"+type+"-media-btmControl-FullScreen-" + callId).on('click', function() {
            var video= $('#'+type+"-media-video-"+ callId);
            if($.isFunction(video[0].webkitEnterFullscreen)) {
			    video[0].webkitEnterFullscreen();
		    }	
            else if($.isFunction(video[0].requestFullscreen)) {
			    video[0].requestFullscreen();
		    }	 
		    else if ($.isFunction(video[0].mozRequestFullScreen)) {
		    	video[0].mozRequestFullScreen();
	    	}
		    else {
			    alert('Your browsers doesn\'t support fullscreen');
		    }
	    });
    }
    function addLocalStream(stream, callId) {
        localStream=stream;
        localCallid=callId;
        $("#local-media-" + callId).remove();
        // Get media container
        var listevideoMini = document.getElementById("ListeVideoMini");
        var div;
        div=document.createElement("div");
        div.className="videoContainer available";
        div.id="local-media-" + callId;
        listevideoMini.appendChild(div);

        // Create media element
        var mediaElement = document.createElement('video');
        mediaElement.id = 'local-media-video-' + callId;
        mediaElement.autoplay = true;
        mediaElement.muted = true;
        mediaElement.className="local-media-video";
        mediaElement.pre="auto";

        // Add media element to media container
        div.appendChild(mediaElement);

        // Attach stream
        stream.attachToElement(mediaElement);

        initStatStream(callId,"local")

        addStreamCaption(stream,callId,TYPE_LOCAL)
        addActionButton(stream,callId,TYPE_LOCAL)
    }

    function createStream(connectedConversation) {
        navigator.mediaDevices.enumerateDevices()
        .then(function(devices) {
          devices.forEach(function(device) {
            trace(device.kind + ": " + device.label +
                        " id = " + device.deviceId);
          });
        })
        .catch(function(err) {
            trace(err.name + ": " + err.message);
        });
       /* navigator.mediaDevices.getUserMedia({ audio: true}).then(function(stream) {
            console.error(stream);
        });
        */
        //==============================
        // CREATE LOCAL STREAM
        //==============================
        //console.log(ua);
        ua.createStream({enhancedAudioActivated : true})
            .then(function (stream) {
                console.log("stream :", stream);

                //==============================
                // PUBLISH OWN STREAM
                //==============================
                connectedConversation.publish(stream, null)
                    .then(function (stream) {
                        console.log('stream is published :', stream);
                        var call = connectedConversation.getConversationCall(stream);
                        console.log('publish::call :', call);
                        addLocalStream(stream, call.callId);
                    })
                    .catch(function (err) {
                        console.error('create stream error', err);
                    });
                })
                .catch(function (err) {
                    console.error('create stream error', err);
                });
    }

    function sendManageDeviceListe(senderId)
    {
        console.log("sendManageDeviceListe",senderId)
        var mediaDevices = ua.getUserMediaDevices();
        var contact = connectedSession.getContact(senderId);
        if (contact !== null) {
            contact.sendData({
                order: 'manageDeviceListe',
                mediaDevices: mediaDevices,
                senderId: connectedSession.getId(),
            });
        }
    }

    function sendPhotoToContact(contactId, media) {
        var contact = connectedSession.getContact(contactId);
        if (contact !== null) {
            var fileInfo = {
                name: 'testPhoto',
                type: 'image/png-dataUrl'
            };
            contact.sendFile(
                fileInfo,
                media
            );
            contact.on('fileTransferProgress', (info) => {
                $('#file-transfer-progress').attr("style", 'width: ' + info.transferInformation.percentage + '%');
            });
        }
    }

    function processReceivedData(e) {
        console.log("processReceivedData :", e.content);
        switch (e.content.order)
        {
            case "takeSnapshot":
                var streamId = connectedConversation.callIdToStreamId.get(e.content.callId.toString());
                apiRTC.Stream.getStream(streamId).takeSnapshot()
                    .then((media) => {
                        sendPhotoToContact(e.content.senderId, media);
                    })
                    .catch((error) => {
                    // error
                    console.error('Error :', error);
                    });
                break;
            case "manageDevice" :
                sendManageDeviceListe(e.content.senderId);
                break;
            case "manageDeviceListe" :
                if(mode==MODE_MEDECIN)
                {
                  updateDeviceList(e.content.mediaDevices,e.content.senderId)
                }
                break;
            case "switchCamera" :
                replaceStream(e.content.id,"camera")
                break;
            case "switchAudio" :
                replaceStream(e.content.id,"audio")
                break;
            case "resfreshStream" :
                replaceStream(0,"none")
                break;
            case "kick" :
                connectedConversation.leave();
                $("#modal-attente").attr('src',"../html/pageAttente.php?kick");
                $("#modal-attente").css("display","block");
                $('.control').css("display","none"); 
                break;
                
    }
}
    /**
     * Create and join a new conference
     * @author Apizee
     *
     * @param {string} name - Conference name
     * @return void
     */
    function joinConference(name) {

//TODO: Creer un compte sur plateforme HDS
     var contact = null;

        //==============================
        // CREATE USER AGENT
        //==============================
        ua = new apiRTC.UserAgent({
//TODO: Creer un compte sur plateforme HDS
             uri: MaltaApz,
        });

        //SELECT_MEDIA
        apiRTC.addEventListener("selectedICECandidate", selectedICECandidateHandler);
        ua.on("mediaDeviceChanged", function (updatedContacts) {
            console.log("mediaDeviceChanged");
            if(connectedSession!=null)
            {
                var contacts =connectedSession.getContactsArray()                
                var i;
               for (i = 0;i<contacts.length; i++) {
                    if(contacts[i].inGroup(conferenceName) && contacts[i].getId()!=connectedSession.getContact(connectedSession.getId()).getId())
                    {
                        sendManageDeviceListe(contacts[i].getId());
                    }
               }            
            }     
        });

        //==============================
        // REGISTER
        //==============================

        ua.register({
            cloudUrl: MaltaCloudUrl,
            ccs : MaltaCcs,
            })
                .then(function (session) {
                    if ((apiCC.browser === 'Chrome') || (apiCC.browser === 'Firefox')) {
                        ua.enableCallStatsMonitoring(true, {interval: 1000});
                    }

                    // Save session
                    connectedSession = session;
                    if(mode==MODE_MEDECIN)
                    {
                        connectedSession.joinGroup("medecin");
                    }
                    else
                    {
                        connectedSession.joinGroup("patient");
                        connectedSession.subscribeToGroup("medecin");
                        
                    }
                    connectedSession.setUsername(username);

                    connectedSession
                        .on("contactListUpdate", function (updatedContacts) { //display a list of connected users
                            console.log("MAIN - contactListUpdate", updatedContacts);
                        })

                        .on("contactData", function (e) {
                            console.log("MAIN - contactData", e);
                            console.log("MAIN - contactData", e.content);
                            processReceivedData(e);
                        })

                        .on('fileTransferInvitation', function (invitation) {
                            console.log("MAIN - fileTransferInvitation");
                            invitation.accept()
                                .then((fileObj) => {
                                    $(".loaderImgSnapshot").removeClass("loaderImgSnapshot").addClass("icon-Snapshot");
                                    console.log("MAIN - fileTransferInvitation file received");
                                    console.log("fileObj :", fileObj);
                                    console.log("name :", fileObj.name);
                                    console.log("type :", fileObj.type);
                                createWhiteBoard(fileObj.file);
                                });     
                        });

                    // CREATE CONVERSATION
                    connectedConversation = connectedSession.getConversation(name);
                    // JOIN CONVERSATION

                    connectedConversation.on('callStatsUpdate', function(callStats) {
                       receiveStat(callStats);
                    });

                    connectedConversation.join()
                        .then(function (response) {
                            createStream(connectedConversation);
                            
                            // WHEN NEW STREAM IS AVAILABLE IN CONVERSATION
                            connectedConversation.on('availableStreamsUpdated', function (streams) {
                                console.log('availableStreamsUpdated::streams : ', streams);
                                var keys = Object.keys(streams),
                                    i = 0,
                                    len = 0;
                                for (i = 0, len = keys.length; i < len; i++) {
                                            afficherStream(streams[keys[i]])
                                }
                            });
                            connectedConversation.on('localStreamUpdated',function(streaminfo){
                                console.error("localStreamUpdated")
                               if(streaminfo.video==false)
                                {
                                    console.error("REFRESHSTREAM")
                                    replaceStream(0,"none")
                                }
                            })
                            connectedConversation.on('remoteStreamUpdated',function(streaminfo){
                                console.error("remoteStreamUpdated")
                                if(streaminfo.video==false)
                                {
                                   

                                    for (var key in subscribedStreams) {
                                        if(subscribedStreams[key]!=undefined)
                                        {
                                            console.error("SEND REFRESHSTREAM")
                                            subscribedStreams[key].contact.sendData({
                                                order: 'resfreshStream'
                                            })
                                        }
                                    }

                                    /*
                                      contacts[i]
                                    */
                                }
                            });
                            // WHEN NEW STREAM IS ADDED TO CONVERSATION
                            connectedConversation.on('streamAdded', function (stream) {
                                console.log('streamAdded::stream : ', stream);
                                    stream.getContact().sendData({
                                        order: 'manageDevice',
                                        senderId: connectedSession.getId()
                                    });

                                    if(window.parent!=null)
                                    {
                                       var message = {
                                        type:"Debut visio",
                                       username:stream.getContact().getUsername()};
                                        window.parent.postMessage(message,window.origin);
                                    }
                                    var listevideoMaxi = document.getElementById("ListeVideoMaxi"),
                                        div,
                                        mediaElement = document.createElement('video');

                                    div=document.createElement("div");
                                    div.className="videoContainer remote";
                                    div.id="remote-media-" + stream.streamId;
                                    ListeVideoMaxi.appendChild(div);
                                    
                                    // Create media element
                                    mediaElement.id = 'remote-media-video-' + stream.streamId;
                                    mediaElement.autoplay = true;
                                    mediaElement.className="myVideo";
                                    mediaElement.pre="auto";

                                    // Add media element to media container
                                    div.appendChild(mediaElement);

                                    // Attach stream
                                    stream.attachToElement(mediaElement);
                                    

                                    initStatStream(stream.callId,"remote")


                                    addStreamCaption(stream,stream.streamId,TYPE_REMOTE)
                                    addActionButton(stream,stream.streamId,TYPE_REMOTE)
                                    console.log("stream.getContact().getId() :", stream.getContact().getId());
                            });

                            // WHEN STREAM WAS REMOVED FROM THE CONVERSATION
                            connectedConversation.on('streamRemoved', function (streamInfos) {
                                subscribedStreams[streamInfos.streamId]=undefined;
                                console.log("streamRemoved",streamInfos);
                                if(window.parent!=null)
                                {
                                   var message = {
                                    type:"Fin visio",
                                   username:streamInfos.getContact().getUsername()};
                                    window.parent.postMessage(message,window.origin);
                                }

                                document.getElementById('remote-media-' + streamInfos.streamId).remove();
                                $("#cameraListe_"+streamInfos.getContact().getId()).remove();
                            }
                            
                        );
                            // WHEN CONTACT JOIN
                            if(mode!=MODE_MEDECIN)
                            {
                            connectedConversation.on('contactJoined', function (contact) {
                                console.log("arrive contact",contact)
                                if(contact.inGroup("medecin"))
                                {
                                    nbContact++
                                    var idcontact=contact.getId()
                                    tabContact[idcontact]=contact
                                    $("#modal-attente").css("display","none");
                                    $('.control').css("display","block"); 
                                }
                                contact.on('joinGroup',function(group){
                                    if(group="medecin")
                                    {
                                        nbContact++
                                        var idcontact=contact.getId()
                                        tabContact[idcontact]=contact
                                        $("#modal-attente").css("display","none");
                                        $('.control').css("display","block"); 
                                    }
                                })
                                contact.on('leaveGroup',function(group){
                                    if(group="medecin")
                                    {
                                        if(typeof tabContact[contact.getId()] !='undefined')
                                        {
                                            tabContact[contact.getId()]=null
                                            nbContact--;
                                            if(nbContact<1)
                                            {
                                               // $('.control').css("display","none"); 
                                                //$("#modal-attente").attr('src',"../html/pageAttente.php?leave&tel="+telMedecin);
                                                //$("#modal-attente").css("display","block");
                                            }
                                            else
                                            {
                                                $('.control').css("display","block"); 
                                            }
                                        }
                                    }
                                })

                            }
                            );

                            // WHEN CONTACT Leave
                            connectedConversation.on('contactLeft', function (contact) {
                            console.log("depart contact",contact)
                                    if(typeof tabContact[contact.getId()] !='undefined')
                                    {
                                        tabContact[contact.getId()]=null
                                        nbContact--;
                                        if(nbContact<1)
                                        {
                                            //TODO remettre le controle quand le client ne se déconnectera plus 
                                            //$('.control').css("display","none"); 
                                            //$("#modal-attente").attr('src',"../html/pageAttente.php?leave&tel="+telMedecin);
                                            //$("#modal-attente").css("display","block");
                                        }
                                        else
                                        {
                                            $('.control').css("display","block"); 
                                        }
                                    }
                                }
                            );
                        }
             
                    }
                );
            }
        );
    }

    joinConference(conferenceName);
    

     //==============================
    // TOOL FUNCTIONS
    //==============================
    function afficherStream(streamInfo){

        if (streamInfo.isRemote === true && typeof subscribedStreams[streamInfo.streamId] === 'undefined') {
            if(mode!=MODE_MEDECIN)
            {
                    console.log("streamInfo.contact.groups",streamInfo.contact.groups)
                    var bokGroup=false;
                    if(mode==MODE_MEDECIN)
                    {
                        bokGroup=true;
                    }
                    else
                    {
                        for (var i = 0; i < streamInfo.contact.groups.length; i++ ) {
                            if(streamInfo.contact.groups[i]==MODE_MEDECIN)
                            {
                                bokGroup=true;
                            }
                        }
                    }

                    if (bokGroup==true)
                    {
                        subscribedStreams[streamInfo.streamId] = streamInfo;
                        connectedConversation.subscribeToMedia(streamInfo.streamId);
                    }
               
            }
            else
            {   
                subscribedStreams[streamInfo.streamId] = streamInfo;
                connectedConversation.subscribeToMedia(streamInfo.streamId);
            }
        }
    }


    //==============================
    // WHITEBOARD FUNCTIONS
    //==============================
    function saveWhiteBoard(numero){
        if(numero>0){
            $("#ListeSnapshotSave-img-"+numero).remove();
        }
        if($("#whiteBoardClient").length>0 )
        {   
    
        //1. récupérer le canvas en cours (les dessins quoi)
            //2. récupérer l'image du background
            //3. dessiner le background sur un canvas
            //4. dessiner le canvas en cours sur le canvas
            //5. récupérer l'image
            //6. ???
            //7. profit
            var canvas = document.getElementById("whiteBoardClient");

            var context = canvas.getContext ? canvas.getContext("2d") : null;
            var canvas2 = document.createElement("canvas");
            canvas2.width = canvas.width;
            canvas2.height = canvas.height;
            var context2 = canvas2.getContext ? canvas2.getContext("2d") : null;
            var imgBg = new Image();
            imgBg.src = bkgPhoto;
            //je retail l'image pour quel rentre dans le canvas en respectant le ratio
            var ratioImageH=imgBg.height/imgBg.width
            var ratioImageW=imgBg.width/imgBg.height
    
            //var ratioCanvas=canvas.height/canvas.width

            var heightImage=0
            var widthImage=0
            var decalageWidth=0
            var decalageHeight=0
            
            if(Math.abs(imgBg.height-canvas.height)>Math.abs(imgBg.width-canvas.width))
            {
                heightImage=canvas.height
                
                widthImage=heightImage*ratioImageW

                
                decalageWidth=Math.abs(canvas.width-widthImage)
                decalageHeight=0
                decalageWidth=decalageWidth/2
            }
            else
            {
                if(Math.abs(imgBg.height-canvas.height)<Math.abs(imgBg.width-canvas.width))
                {
                    widthImage=canvas.width

                    heightImage=canvas.width*ratioImageH    

                    decalageHeight=Math.abs(canvas.height-heightImage)
                    decalageWidth=0
                    decalageHeight=decalageHeight/2
                }
                else
                {
                        heightImage=canvas.height
                        widthImage=canvas.width
                }
   
            }




            context2.drawImage(canvas,0,0, canvas.width,canvas.height);
            context.clearRect(0,0,canvas.width,canvas.height);
            context.fillStyle='black'
            context.fillRect(0, 0, canvas.width, canvas.height);

            context.drawImage(imgBg,decalageWidth,decalageHeight,widthImage, heightImage);
            context.drawImage(canvas2, 0, 0, canvas.width, canvas.height);
            
            var dataURL = canvas.toDataURL();
            
            var newImage=document.createElement("img");
            newImage.src=dataURL
            newImage.className="ListeSnapshotSave-img"
            
            numeroImage++;
            var numeroEnCours=numeroImage;
            newImage.id='ListeSnapshotSave-img-'+numeroEnCours

            newImage.setAttribute("idImage",canvas.getAttribute("idImage"))
            var ListeSnapshotSave = document.getElementById("ListeSnapshotSave")
            ListeSnapshotSave.appendChild(newImage);
            $("#ListeSnapshotSave-img-"+numeroEnCours).click(function(){
               createWhiteBoard(newImage.src,numeroEnCours);
            });
            $("#whiteBoardActif").remove();
            //window.parent.postMessage("newImage","*")
        }
    }

    function addButtonWhiteBoardUndo(){
        var whiteboardLeft = document.getElementById("whiteboardLeft"),
            undoButton = document.createElement("div"),
            spanUndo=document.createElement("span");

        undoButton.id="WhiteBoardButton-undo";
        undoButton.title="Annuler"
        undoButton.className="WBUndo";
        whiteboardLeft.appendChild(undoButton);
        spanUndo.className="icon-undo";
        undoButton.appendChild(spanUndo);
        $('#WhiteBoardButton-undo').click(function () {
            whiteBoardClient.undo();
        });
    }

    function addButtonWhiteBoardRedo(){
        var whiteboardLeft = document.getElementById("whiteboardLeft"),
            redoButton = document.createElement("div"),
            spanRedo=document.createElement("span");

        redoButton.id="WhiteBoardButton-redo";
        redoButton.className="WBUndo";
        redoButton.title="Rétablir";
        whiteboardLeft.appendChild(redoButton);
        spanRedo.className="icon-redo";
        redoButton.appendChild(spanRedo);
        $('#WhiteBoardButton-redo').click(function () {
            whiteBoardClient.redo();
        });
        }

    function addButtonWhiteBoardRectangle(){
        var whiteboardLeft = document.getElementById("whiteboardLeft"),
            rectangle = document.createElement("div"),
            spanRectangle=document.createElement("span");
    
        rectangle.id="WhiteBoardButton-rectangle";
        rectangle.className="WBRectangle";
        rectangle.title="Outil rectangle";
        whiteboardLeft.appendChild(rectangle);

        spanRectangle.className="icon-rectangle";
        rectangle.appendChild(spanRectangle);

        $('#WhiteBoardButton-rectangle').click(function () {
            whiteBoardClient.setDrawingTool("rectangle");
            buttonWhiteBoardSelect("WhiteBoardButton-rectangle");
        });

    }

    function addButtonWhiteBoardPen(){
        var whiteboardLeft = document.getElementById("whiteboardLeft"),
            pen = document.createElement("div"),
            spanPen=document.createElement("span");

        pen.id="WhiteBoardButton-pen";
        pen.className="WBLine";
        pen.title="Outil stylo";
        whiteboardLeft.appendChild(pen);

        spanPen.className="icon-line";
        pen.appendChild(spanPen);

        $('#WhiteBoardButton-pen').click(function () {
            whiteBoardClient.setDrawingTool("pen");
            buttonWhiteBoardSelect("WhiteBoardButton-pen");
        });


    }

    function addButtonWhiteBoardCircle(){
        var whiteboardLeft = document.getElementById("whiteboardLeft"),
            circle = document.createElement("div"),
            spanCircle=document.createElement("span");

        circle.id="WhiteBoardButton-circle";
        circle.className="WBCircle";
        circle.title="Outil rond";
        whiteboardLeft.appendChild(circle);

        spanCircle.className="icon-circle";
        circle.appendChild(spanCircle);

        $('#WhiteBoardButton-circle').click(function () {
            whiteBoardClient.setDrawingTool("ellipse");
            buttonWhiteBoardSelect("WhiteBoardButton-circle");
        });

    }

    function addButtonWhiteBoardArrow(){
        var whiteboardLeft = document.getElementById("whiteboardLeft"),
            arrow = document.createElement("div"),
            spanArrow=document.createElement("span");
        
        arrow.id="WhiteBoardButton-arrow";
        arrow.className="WBArrow";
        arrow.title="Outil flêche";
        whiteboardLeft.appendChild(arrow);

        spanArrow.className="icon-arrow";
        arrow.appendChild(spanArrow);

        $('#WhiteBoardButton-arrow').click(function () {
            whiteBoardClient.setDrawingTool("arrow");
            buttonWhiteBoardSelect("WhiteBoardButton-arrow");
        });
    }

    function addButtonWhiteBoardColorPicker(){
        var whiteboardLeft = document.getElementById("whiteboardLeft"),
            ColorPicker = document.createElement("div"),
            spanColorPicker=document.createElement("span"),
            divColorPicker =document.createElement("div"),
            colorList = [ '000000', '993300', '333300', '003300', '003366', '000066', '333399', '333333', 
            '660000', 'FF6633', '666633', '336633', '336666', '0066FF', '666699', '666666', 'CC3333', 'FF9933', '99CC33', '669966', '66CCCC', '3366FF', '663366', '999999', 'CC66FF', 'FFCC33', 'FFFF66', '99FF66', '99CCCC', '66CCFF', '993366', 'CCCCCC', 'FF99CC', 'FFCC99', 'FFFF99', 'CCffCC', 'CCFFff', '99CCFF', 'CC99FF', 'FFFFFF' ],
            picker = null;

        ColorPicker.id="WhiteBoardButton-ColorPicker";
        ColorPicker.className="WBColorPicker";
        ColorPicker.title="Couleur";
        whiteboardLeft.appendChild(ColorPicker);
        
        spanColorPicker.className="icon-colorpicker";
        ColorPicker.appendChild(spanColorPicker);
        
        divColorPicker.id="color-picker";
        divColorPicker.className="color-picker";
        divColorPicker.style="display:none";
        ColorPicker.appendChild(divColorPicker);
        picker=$('#color-picker');

        //==============================
        // COLOR PICKER
        //==============================
    
        for (var i = 0; i < colorList.length; i++ ) {
            picker.append('<li class="color-item" data-hex="' + '#' + colorList[i] + '" style="background-color:' + '#' + colorList[i] + ';"></li>');
        }

        $('body').click(function () {
            picker.fadeOut();
        });

        $('.color-item').click(function (event) {
            //selection
            var codeHex = $(this).data('hex'),
                wbcolor=document.getElementById("WhiteBoardButton-ColorPicker");
            
            picker.fadeOut();
            event.stopPropagation();
            FontColor=codeHex;      
            wbcolor.style.borderColor=FontColor;
            whiteBoardClient.setBrushColor(FontColor);
        });

        $('.WBColorPicker').click(function(event) {
            event.stopPropagation();
            picker.fadeIn();
        });
    }

    function buttonWhiteBoardSelect(idButton){
        $('.whiteboardLeft').children().removeClass('toolSelected');
        $('#'+idButton).toggleClass('toolSelected');
    }

    function addButtonWhiteBoardReduce() {
        var whiteboardLeft = document.getElementById("whiteboardLeft"),
            reduce = document.createElement("div"),
            spanReduce=document.createElement("span");

        reduce.id="WhiteBoardButton-reduce";
        reduce.className="WBCollapse";
        reduce.title="Réduire";
        whiteboardLeft.appendChild(reduce);

        spanReduce.className="icon-collapse";
        reduce.appendChild(spanReduce);

        $('#WhiteBoardButton-reduce').click(function () {
            saveWhiteBoard(0);
        });
    }

    function addButtonWhiteBoardDelete() {
        var whiteboardLeft = document.getElementById("whiteboardLeft"),
            deleteButton = document.createElement("div"),
            spanDelete=document.createElement("span");

        deleteButton.id="WhiteBoardButton-delete";
        deleteButton.className="WBDelete";
        deleteButton.title="Supprimer";
        whiteboardLeft.appendChild(deleteButton);

        spanDelete.className="icon-delete";
        deleteButton.appendChild(spanDelete);

        $('#WhiteBoardButton-delete').click(function () {
            $("#whiteBoardActif").remove();
        });
    }

    function addButtonWhiteBoardSelectPosition(){
        var whiteboardLeft = document.getElementById("whiteboardLeft"),
            selectText = document.createElement("div"),
            spanselectText=document.createElement("span");

        selectText.id="WhiteBoardButton-selectText";
        selectText.className="WBText";
        selectText.title="Insérer du texte";
        whiteboardLeft.appendChild(selectText);

        spanselectText.className="icon-text";
        selectText.appendChild(spanselectText);

        $('#WhiteBoardButton-selectText').click(function () {
            whiteBoardClient.setDrawingTool("void");
            buttonWhiteBoardSelect("WhiteBoardButton-selectText");
            $("#whiteBoardClient").attr("curseur","true");
        });

        $('#whiteBoardClient').click(function (e) {
            if($("#whiteBoardClient").attr("curseur")=="true")
            { 
                var modal = document.getElementById("myModal");          
                var parentOffset = $(this)
                    .parent()
                    .offset();
                xEncours = (e.pageX - parentOffset.left) ; //+  $(window).scrollLeft();
                yEncours = (e.pageY - parentOffset.top) ;// +  $(window).scrollTop();
                modal.style.display = "block";
                modal.style.top = yEncours+'px';
                modal.style.left= xEncours+'px';
                modal.style.position = 'absolute';
                
                $("#inputSaisieTexte").val("");
                var monInputTexte = document.getElementById("inputSaisieTexte");
                
                monInputTexte.focus();
                event.stopPropagation();
            }
        });
        
        $("#inputSaisieTexteValider").click(function(e){
            e.preventDefault();
            writeWhiteBoardText()
        });

    }


    
    function writeWhiteBoardText(){
        var modal = document.getElementById("myModal");
        console.log("value:",xEncours);
        console.log("value:",yEncours);
        whiteBoardClient.printSharedText(xEncours,yEncours+10,$("#inputSaisieTexte").val(),20);
        $("#whiteBoardClient").attr("curseur","false");
        whiteBoardClient.setDrawingTool("pen");
        buttonWhiteBoardSelect("WhiteBoardButton-pen");
    }

    function createWhiteBoardCaption(){
        var listeWhiteBord = document.getElementById("whiteBoardActif"),
            caption =document.createElement("div");

        caption.className="caption whiteboard";
        caption.innerText="Tableau blanc";
        caption.id="WhiteBoard-Caption";

        listeWhiteBord.appendChild(caption);
    }

    function createWhiteBoardActionBarre(){
        createWhiteBoardActionBarreLeft();
        //createWhiteBoardActionBarreRight();
        buttonWhiteBoardSelect("WhiteBoardButton-pen");
    }
      
    function createWhiteBoardActionBarreLeft(){
        var listeWhiteBord = document.getElementById("whiteBoardActif"),
            whiteboardLeft = document.createElement("div");

        whiteboardLeft.className="whiteboardLeft";
        whiteboardLeft.id="whiteboardLeft";
        listeWhiteBord.appendChild(whiteboardLeft);
        addButtonWhiteBoardPen();
        addButtonWhiteBoardRectangle();
        addButtonWhiteBoardCircle();
        addButtonWhiteBoardArrow();
        addButtonWhiteBoardSelectPosition();
        addButtonWhiteBoardColorPicker();

        addButtonWhiteBoardUndo();
        addButtonWhiteBoardRedo();
        addButtonWhiteBoardReduce();
        addButtonWhiteBoardDelete();
    }
    
    
    function createWhiteBoard(file,numero=0){
        var numImg =0 ;
        numeroImage++
        if(numero>0)
        {
            numImg= $("#ListeSnapshotSave-img-"+numero).attr("idImage")

        }
        else
        {
            numImg=Date.now()*numeroImage
        }

        saveWhiteBoard(numero);
        console.log('startOfflineWhiteboard');

        var listeWhiteBord = document.getElementById("ListeWhiteBoard"),
            div =document.createElement("div"),
            canvas = document.createElement("canvas"),
            height=0,
            divModal =document.createElement("div"),
            formModal = document.createElement("form"),
            inputModal = document.createElement("input"),
            buttonModal = document.createElement("button");

        div.id="whiteBoardActif";
        div.className="videoContainer remote whiteboard";
        listeWhiteBord.appendChild(div);
        
        divModal.id="myModal";
        divModal.style="margin:0;padding:0;"
        divModal.hidden=true;
        divModal.className="modal-content";
        div.appendChild(divModal);

        formModal.id="modal-form";
        formModal.className="modal-form";
        formModal.style="margin-block-end: auto"
        formModal.action="";
        formModal.onsubmit="";
        divModal.appendChild(formModal);

        inputModal.id="inputSaisieTexte";
        inputModal.type="textarea";
        formModal.appendChild(inputModal);

        buttonModal.id="inputSaisieTexteValider";
        buttonModal.type="submit";
        buttonModal.innerText="OK";
        formModal.appendChild(buttonModal);

        canvas.setAttribute("idImage",numImg)
        canvas.id="whiteBoardClient";
        canvas.className="myVideo";
        div.appendChild(canvas);

        height=$("#whiteBoardClient").height()-25;
        $("#whiteBoardClient").height(height);

        canvas.width = $("#whiteBoardClient").width();
        canvas.height = $("#whiteBoardClient").height();
      
        bkgPhoto = file;
        canvas.style.backgroundImage = 'url(' + bkgPhoto + ')';
        numWhiteBoard++;

        ua.stopWhiteboard();
        ua.startWhiteboard("whiteBoardClient");

        whiteBoardClient = ua.getWhiteboardClient();
        whiteBoardClient.setBrushSize(3);
        console.log("whiteBoardClient",whiteBoardClient)
        whiteBoardClient.setFocusOnDrawing(true);

        createWhiteBoardCaption();
        createWhiteBoardActionBarre();
    }
//infoStream
function initStatStream(callid,type){
    if(type=="remote")
    {
        if (!qosStats[callid]) {
            qosStats[callid] = {
                'mosRV': 0,             // Received video quality
                'mosRS': 0,             // Received audio quality
                'videoRBR': 0,          // Received video bit rate
                'audioRBR': 0,          // Received audio bit rate
                'videoRFR': 0,          // Received video frame rate
                'videoRPL': 0,          // Received video packet loss rate
                'audioRPL': 0,          // Received audio packet loss rate
                'videoRHeight': 0,      // Received video height
                'videoRWidth': 0,       // Received video width
                'transportType': 'ND',  // Transport type
            };
        }
    }
    else
    {
        if (!qosStats[0]) {
            qosStats[0] = {
                'mosSV': 0,             // Sent video quality
                'mosSS': 0,             // Sent audio quality
                'videoSBR': 0,          // Sent video bit rate
                'audioSBR': 0,          // Sent audio bit rate
                'videoSPL': 0,          // Sent video packet loss rate
                'audioSPL': 0,          // Sent audio packet loss rate
                'videoSFR': 0,          // Sent video frame rate
                'videoSHeight': 0,      // Sent video height
                'videoSWidth': 0,       // Sent video width
                'transportType': 'ND',  // Transport type
            };
        }
    }

}

function selectedICECandidateHandler(e) {
    if(qosStats[e.detail.callId] !== undefined) {
        qosStats[e.detail.callId].transportType = e.detail.transportType;
    }
}
function receiveStat(callStats){
    //reception QoS statistics
    if (callStats.stats.videoReceived || callStats.stats.audioReceived ){
        if (callStats.stats.videoReceived){
            qosStats[callStats.callId].videoRBR = Math.round(callStats.stats.videoReceived.bitsReceivedPerSecond /1000) ; //Kbps
            qosStats[callStats.callId].videoRFR = callStats.stats.videoReceived.framesDecodedPerSecond;
            qosStats[callStats.callId].videoRPL = callStats.stats.videoReceived.packetsLostRatio;
            qosStats[callStats.callId].videoRHeight = callStats.stats.videoReceived.height;
            qosStats[callStats.callId].videoRWidth = callStats.stats.videoReceived.width;
            qosStats[callStats.callId].mosRV = callStats.stats.quality.mosV;

        }
        if (callStats.stats.audioReceived) {
            qosStats[callStats.callId].audioRBR = Math.round(callStats.stats.audioReceived.bitsReceivedPerSecond /1000) ; //Kbps
            qosStats[callStats.callId].audioRPL = callStats.stats.audioReceived.packetsLostRatio;
            qosStats[callStats.callId].mosRS = callStats.stats.quality.mosS;
        }
    }
    //send QoS statistics
    if (callStats.stats.videoSent || callStats.stats.audioSent ){
        if (callStats.stats.videoSent) {
            if(!qosStats[0])
            {
                initStatStream(0,"local")
            }
            qosStats[0].videoSBR = Math.round(callStats.stats.videoSent.bitsSentPerSecond /1000); //Kbps
            qosStats[0].videoSFR = callStats.stats.videoSent.framesEncodedPerSecond;
            qosStats[0].videoSPL = callStats.stats.videoSent.packetLossRatio;
            qosStats[0].videoSHeight = callStats.stats.videoSent.height;
            qosStats[0].videoSWidth = callStats.stats.videoSent.width;
            qosStats[0].mosSV = callStats.stats.quality.mosSV;

        }
        if (callStats.stats.audioSent) {
            qosStats[0].audioSBR = Math.round(callStats.stats.audioSent.bitsSentPerSecond /1000);
            qosStats[0].audioSPL = callStats.stats.audioSent.packetLossRatio;
            qosStats[0].mosSS = callStats.stats.quality.mosSS;
        }
    }
    ViewStat(qosStats[callStats.callId],qosStats[0])
}
function ViewStat(remoteStat,localStat)
{
    var divType = document.createElement("div"),
       
        divVideo= document.createElement("div"),
        divVideoQ=document.createElement("div"),
        divVideoBR=document.createElement("div"),
        divVideoFR=document.createElement("div"),
        divVideoPP=document.createElement("div"),
        divVideoR=document.createElement("div"),
        
        divAudio=document.createElement("div"),
        divAudioQ=document.createElement("div"),
        divAudioBR=document.createElement("div"),
        divAudioPP=document.createElement("div"),
        divTransport=document.createElement("div"),
        divTransportT=document.createElement("div"),
        divInfoTextLocal=document.createElement("div"),
        divInfoTextRemote=document.createElement("div"),

        divAutre=document.createElement("div"),
        divResolution=document.createElement("div"),
        divHauteur=document.createElement("div"),
        divLargeur=document.createElement("div"),
        divAgrandissement=document.createElement("div");


        
        divType.className="modal-header"
        divVideo.className="modal-title"
        divVideoQ.className="modal-option"
        divVideoBR.className="modal-option"
        divVideoFR.className="modal-option"
        divVideoPP.className="modal-option"
        divVideoR.className="modal-option"

        divAudio.className="modal-title"
        divAudioQ.className="modal-option"
        divAudioBR.className="modal-option"
        divAudioPP.className="modal-option"
        
        divTransport.className="modal-title"
        divTransportT.className="modal-option"

        divInfoTextLocal.className="modal-body"
        divInfoTextRemote.className="modal-body"

        divResolution.className="modal-title"
        divHauteur.className="modal-option"
        divLargeur.className="modal-option"
        divAgrandissement.className="modal-option"

    if(typeof(localStat)!="undefined")
    {
        divType.innerText="Local"
        divVideo.innerText="Vidéo"
        if (localStat.videoSBR != 0) {
            if (localStat.mosSV >= 4 ){
                divVideoQ.innerText="qualité : excellente"
            } else if (localStat.mosSV >= 3 ){
                divVideoQ.innerText="qualité : bonne"
            } else if (localStat.mosSV >= 2 ){
                divVideoQ.innerText="qualité : suffisante"
            } else {
                divVideoQ.innerText="qualité : unsuffisante";
            }
            divVideoBR.innerText='bit rate : ' + localStat.videoSBR + ' Kbps';
            divVideoFR.innerText='frame rate : ' + localStat.videoSFR;
            divVideoPP.innerText='perte de paquet : ' + localStat.videoSPL.toFixed(2)  + '%';
            divVideoR.innerText='resolution : ' + localStat.videoSWidth + '×' + localStat.videoSHeight;
        } else {
            divVideo.innerText='Pas de vidéo'
        }
        divAudio.innerText="Audio"
        if (localStat.audioSBR != 0){
            if (localStat.mosSS >= 4 ){
                divAudioQ.innerText="qualité : excellente"
            } else if (localStat.mosSS >= 3 ){
                divAudioQ.innerText="qualité : bonne"
            } else if (localStat.mosSS >= 2 ){
                divAudioQ.innerText="qualité : suffisante"
            } else {
                divAudioQ.innerText="qualité : unsuffisante"
            }
            divAudioBR.innerText='bit rate : ' + localStat.audioSBR
            divAudioPP.innerText=' perte de paquet : ' + localStat.audioSPL.toFixed(2)  + '%'

        }else {
            divAudio.innerText="Pas d'audio"
        }
        divResolution.innerText="Résolution"
       // divHauteur.innerText="Hauteur : ".window.screen.height
        divLargeur.innerText="Largeur : "+screen.width
        divHauteur.innerText="Hauteur : "+screen.height
        divAgrandissement.innerText="Aggrandissement : "+Math.round(devicePixelRatio*100)+" %"

        

        divInfoTextLocal.innerHTML=""

        divInfoTextLocal.appendChild(divType);
        divInfoTextLocal.appendChild(divVideo);
        if(divVideo.innerText=="Vidéo")
        {
            divInfoTextLocal.appendChild(divVideoQ);
            divInfoTextLocal.appendChild(divVideoBR);
            divInfoTextLocal.appendChild(divVideoFR);
            divInfoTextLocal.appendChild(divVideoPP);
            divInfoTextLocal.appendChild(divVideoR);
        }
        divInfoTextLocal.appendChild(divAudio);
        if(divAudio.innerText=="Audio")
        {
            divInfoTextLocal.appendChild(divAudioQ);
            divInfoTextLocal.appendChild(divAudioBR);
            divInfoTextLocal.appendChild(divAudioPP);
        }
        divInfoTextLocal.appendChild(divResolution);
        divInfoTextLocal.appendChild(divLargeur)
        divInfoTextLocal.appendChild(divHauteur)
        divInfoTextLocal.appendChild(divAgrandissement)
        var modalLocal=document.getElementById("modal-content-info-local")
        modalLocal.innerHTML=divInfoTextLocal.innerHTML;
    }
    
    if(typeof(remoteStat)!="undefined")
    {
        divType.innerText="Distant"
            divVideo.innerText="Vidéo"
                if (remoteStat.videoRBR != 0){
                    if (remoteStat.mosRV >= 4 ){
                        divVideoQ.innerText="qualité : Excellente";
                    } else if (remoteStat.mosRV >= 3 ){
                        divVideoQ.innerText="qualité : bonne ";
                    } else if (remoteStat.mosRV >= 2 ){
                        divVideoQ.innerText="qualité : suffisante";
                    } else {
                        divVideoQ.innerText="qualité : unsuffisante";
                    }
                    divVideoBR.innerText="bite rate : "+remoteStat.videoRBR +" Kbps";
                    divVideoFR.innerText="frame rate : "+remoteStat.videoRFR;
                    divVideoPP.innerText="perte de paquet : "+remoteStat.videoRPL.toFixed(2) + ' %'
                    divVideoR.innerText='resolution : ' + remoteStat.videoRWidth + '×' + remoteStat.videoRHeight
                }
                else
                {
                    divVideo.innerText="Pas de vidéo";
                }
                // Audio stream is received
        if (remoteStat.audioRBR != 0){
            divAudio.innerText="Audio"
            if (remoteStat.mosRS >= 4 ){
                divAudioQ.innerText="qualité : Excellente";
            } else if (remoteStat.mosRS >= 3 ){
                divAudioQ.innerText="qualité : bonne";
            } else if (remoteStat.mosRS >= 2 ){
                divAudioQ.innerText="qualité : suffisante";
            } else {
                divAudioQ.innerText="qualité : unsuffisante ";
            }
            divAudioBR.innerText="bit rate : " + remoteStat.audioRBR + " Kbps";
            divAudioPP.innerText='perte de paquet :  ' + remoteStat.audioRPL.toFixed(2) + ' %'
        } else {
            divAudio.innerText="pas d'audio"
        }
        divTransport.innerText='Transport';
        divTransportT.innerText='type : '+ remoteStat.transportType;
    
        divInfoTextRemote.innerHTML=""
        divInfoTextRemote.appendChild(divType);
        divInfoTextRemote.appendChild(divVideo);
        if(divVideo.innerText=="Vidéo")
        {
            divInfoTextRemote.appendChild(divVideoQ);
            divInfoTextRemote.appendChild(divVideoBR);
            divInfoTextRemote.appendChild(divVideoFR);
            divInfoTextRemote.appendChild(divVideoPP);
            divInfoTextRemote.appendChild(divVideoR);
        }
        divInfoTextRemote.appendChild(divAudio);
        if(divAudio.innerText=="Audio")
        {
            divInfoTextRemote.appendChild(divAudioQ);
            divInfoTextRemote.appendChild(divAudioBR);
            divInfoTextRemote.appendChild(divAudioPP);
        }
        divInfoTextRemote.appendChild(divTransport);
        divInfoTextRemote.appendChild(divTransportT);
        var modalRemote=document.getElementById("modal-content-info-remote")
        modalRemote.innerHTML=divInfoTextRemote.innerHTML;
    }
    
}
    
  //==============================
    // MODAL
    //==============================
    if(mode==MODE_MEDECIN)
    {
        window.onclick = function(event) {
            var modal = document.getElementById("myModal");
            if (typeof(modal) != 'undefined' && modal != null)
            {
                if (modal.style.display != "none") {
                    modal.style.display = "none";
                    $("#whiteBoardClient").attr("curseur","false");
                    whiteBoardClient.setDrawingTool("pen");
                    buttonWhiteBoardSelect("WhiteBoardButton-pen");
                }
            }
        };

    }    
});

function modeMiniature(){
    if(localCallid!=null)
    {
      /*  
        $(".remote" + localCallid).addClass("frame");
        $("#local-media-video" + localCallid).addClass("frame");
        $("#ListeVideoMini").addClass("frame");
        $("#local-media-caption-" + localCallid).css("display","none");
        $(".remote").css("display","none");
    */
   $(".remote").addClass("frame");
   $(".myVideo").addClass("frame");
   $("#ListeVideoMaxi").addClass("frame");
   $("#ListeVideoMini").css("display","none");
    }
}

function modeNormal(){
    if(localCallid!=null)
    {
        /*
        $("#local-media-" + localCallid).removeClass("frame");
        $("#local-media-video" + localCallid).removeClass("frame");
        $("#ListeVideoMini").removeClass("frame");

        $("#local-media-caption-" + localCallid).css("display","block");
        $(".remote").css("display","block");
        */
       $(".remote").removeClass("frame");
       $(".myVideo").removeClass("frame");
       $("#ListeVideoMaxi").removeClass("frame");
       $("#ListeVideoMini").css("display","block");
    }
}

function kickPatient(p_n_dossier){
    if(connectedSession!=null)
    {
    var contacts =connectedSession.getContactsArray()                
    var i;
        for (i = 0;i<contacts.length; i++) {
            if (contacts[i].getUsername().indexOf("|")!=-1) 
            {
                var n_dossier=contacts[i].getUsername().split("|")[0];
                if (n_dossier==p_n_dossier){
                    contacts[i].sendData({
                        order: 'kick'
                    });

                }
            }         
        }  
    }
}


//==============================
// FONCTION NON IMPLEMENTE
//==============================
/*

/*$("#createStreamAudio").click(function () {
    var option = {audioInputId: selectMic.value, videoInputId: false};
   
    console.log('MAIN - Click createStreamAudio');
    createStream(connectedConversation, option);
});
*/


/*
$("#leaveConversation").click(function () {
    var key = null;

    console.log('MAIN - Click leaveConversation');
    connectedConversation.leave();
    console.log(apiRTC.Stream.getLocalStreams());
    for (key of apiRTC.Stream.getLocalStreams()) {
        key.release();
        console.log(key.getId());
        $("#local-media-" + key.getId()).remove();
        $("#unpublish-" + key.getId()).remove();
    }
});
*/



/*
ua.fetchNetworkInformation(cloudUrl)
    .then((e) => {
        console.error("fetchNetworkInformation OK", e);
    })
    .catch(() => {
        console.log("fetchNetworkInformation NOK");
});
*/
//==============================
// RECORDING FUNCTIONS
//==============================
/*
$('#startConversationRecording').click(function () {
    console.log('startConversationRecording');
    //TODO: change usage apiRTC3 to use apiRTC4 API
    apiRTC.session.apiCCWebRTCClient.webRTCClient.MCUClient.startCompositeRecording(
        'media',
        'test_record_malta',
        connectedConversation.getName()
    );
});
*/

/*
$('#stopConversationRecording').click(function () {
    console.log('stopConversationRecording');
    apiRTC.session.apiCCWebRTCClient.webRTCClient.MCUClient.stopCompositeRecording();
});
*/
function trace(pinfo,clear=0)
{
    if(traceActive=="1")
    {
        divLog=document.getElementById("log")
        if(clear==1 && divLog!=null)
        {
            console.error(divLog.innerText)
            divLog.innerHTML='';
        }
        divinfo=document.createElement("div")
        divinfo.innerText=pinfo;
        divLog.appendChild(divinfo);
    }
}