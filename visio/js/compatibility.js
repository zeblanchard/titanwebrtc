$(function () {
    function supportWebRtc()
    {
        //SUPPORTER
            //CHROME >= 45
            //FIREFOX >= 46
            //OPERA >= 35
            //SAFARI >= 11
        var parser=new UAParser();
        var resultParser=parser.getResult();
        switch(resultParser.browser.name)
        {
            case "Chrome" :
                if(resultParser.browser.major>=parseInt(45))
                {
                    return true
                }
                break;
            case "Firefox" :
                if(resultParser.browser.major>=parseInt(46))
                {
                    return true
                }
                break;
            case "Opera" :
                if(resultParser.browser.major>=parseInt(35))
                {
                    return true
                }
                break;
            case "Safari" :
                if(resultParser.browser.major>=parseInt(11))
                {
                    return true
                }
                break;
        }
        return false
    }
    var body=document.getElementsByTagName("BODY")[0];
    if(supportWebRtc()==true){
        var scriptApiRTC =document.createElement("script");
        scriptApiRTC.src="https://cloud.apizee.com/apiRTC/v4.6/apiRTC-4.6.1.min.js";
        scriptApiRTC.type="text/javascript"
        body.appendChild(scriptApiRTC);
        scriptApiRTC.onload=function(){
            var scriptWebRtc =document.createElement("script");
            scriptWebRtc.src="../js/webrtc.js";
            scriptWebRtc.type="text/javascript"
            body.appendChild(scriptWebRtc);
        }
    }
    else
    {
        var teleconsult=document.getElementById("teleconsultation");
        var divNonSupport=document.createElement("div");
        divNonSupport.id="div-nonSupporte";
        divNonSupport.innerHTML="Votre Navigateur ne supporte pas la téléconsultation, merci de vous procurer un des navigateurs suivant :";
        teleconsult.appendChild(divNonSupport);
        var ul=document.createElement("ul")
        ul.id="ul-nonSupporte"
        teleconsult.appendChild(ul);

        var liChrome=document.createElement("li");
        liChrome.className="li-nonSupporte"
        liChrome.innerHTML="Chrome supérieur à la version 45"
        ul.appendChild(liChrome);

        var liFirefox=document.createElement("li");
        liFirefox.className ="li-nonSupporte"
        liFirefox.innerHTML="Firefox supérieur à la version 46"
        ul.appendChild(liFirefox);

        var liOpera=document.createElement("li");
        liOpera.className ="li-nonSupporte"
        liOpera.innerHTML="Opéra supérieur à la version 35"
        ul.appendChild(liOpera);

        var liSafari=document.createElement("li");
        liSafari.className ="li-nonSupporte"
        liSafari.innerHTML="Safari supérieur à la version 11"
        ul.appendChild(liSafari);

        
    }
    
}
);