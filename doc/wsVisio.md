# wsVisio
## Creation de la consultation
* **FONCTION**
    * Créer la consultation
    * logger l'arriver du patient
    * renvoyer l'url de connection à la salle pour le patient
* **URL**
> http://update.malta-informatique.fr/WS/Visio/wsvisio.php?creerConsultation
* **JSON ENTRE : exemple**
```JSON
[ 
    { 
        "Idprescrip":18,
        "n_dossier":458, 
        "RPPS":"PRESCRIP.IDEPRS",
        "MedecinNom":"PRESCRIP.NOM",
        "MedecinPrenom":"PRESCRIP.PRENOM",
        "MedecinPortable":"",
        "MedecinMail":"",
        "PatientNom":"RESIDANT.NOM",
        "PatientPrenom":"RESIDANT.PRENOM",
        "Consentement":0,
        "Finess":"Titan.CODETAB",
        "Etablissement":"Titan.NOM_ETABL Titan.NOM_ETABL2", 
        "UrlWebMedecin":"URL_WEBMEDECIN", 
        "AccompagnantNom":"Operat.NOM", 
        "AccompagnantPrenom":"Operat.PRENOM", 
        "AccompagnantSpecilialite":"GPWSpecialite.libelle_specialit\u00e9" 
    } 
 ]
```
 * **JSON SORTIE : exemple**
```JSON
{
    "statut":"ok",
    "url":"https:\/\/update.malta-informatique.fr\/VISIO\/html\/patient.html?room=UFJFU0NSSVAuSURFUFJTLVRpdGFuLkNPREVUQUI%3D&mode=patient&username=458%7CRESIDANT.NOM+RESIDANT.PRENOM"
}
```
## Début de la consultation
* **FONCTION**
ecrire dans les log que la consultation à démarrée
* **URL**
> http://update.malta-informatique.fr/WS/Visio/wsvisio.php?debutConsultation
* **JSON ENTRE : exemple**
```JSON
[
    { 
        "n_dossier":123,
        "RPPS":"PRESCRIP.IDEPRS",
        "Finess":"Titan.CODETAB"
     }
]
```


## Fin de la consultation
* **FONCTION**
écrire dans les log que la consultation à fini
* **URL**
> http://update.malta-informatique.fr/WS/Visio/wsvisio.php?finConsultation
* **JSON ENTRE : exemple**
```JSON
[
    { 
        "n_dossier":123,
        "RPPS":"PRESCRIP.IDEPRS",
        "Finess":"Titan.CODETAB"
     }
]
```

## Info de la consultation
* **FONCTION**
Renvoyer les info de la consultation
* **URL**
> http://update.malta-informatique.fr/WS/Visio/wsvisio.php?infoConsultation
* **JSON ENTRE : exemple**
```JSON
[
    { 
        "n_dossier":123,
        "RPPS":"PRESCRIP.IDEPRS",
        "Finess":"Titan.CODETAB"
     }
]
```
* **JSON SORTIE : exemple**
```JSON
[
    {
        "IdConsultationLog":3,
        "Idprescrip":18,
        "MedecinMail":"",
        "Medecin":"PRESCRIP.NOM PRESCRIP.PRENOM",
        "MedecinPortable":"",
        "n_dossier":458,
        "Patient":"RESIDANT.NOM RESIDANT.PRENOM",
        "RPPS":"PRESCRIP.IDEPRS",
        "Consentement":1,
        "Finess":"Titan.CODETAB",
        "Etablissement":"Titan.NOM_ETABL Titan.NOM_ETABL2",
        "UrlWebMedecin":"URL_WEBMEDECIN",
        "Accompagnant":"Operat.NOM Operat.PRENOM (GPWSpecialite.libelle_specialit\u00e9)",
        "UrlPatient":"https:\/\/update.malta-informatique.fr\/VISIO\/html\/patient.html?room=UFJFU0NSSVAuSURFUFJTLVRpdGFuLkNPREVUQUI%3D&mode=patient&username=458%7CRESIDANT.NOM+RESIDANT.PRENOM",
        "Date":"08.08.18"
    }
]
```

## Log de la consultation
* **FONCTION**
Renvoyer les log de la consultation
* **URL**
> http://update.malta-informatique.fr/WS/Visio/wsvisio.php?logConsultation
* **JSON ENTRE : exemple**
```JSON
[
    { 
        "n_dossier":123,
        "RPPS":"PRESCRIP.IDEPRS",
        "Finess":"Titan.CODETAB"
     }
]
```
* **JSON SORTIE : exemple**
```JSON
[
    {
        "idconsultation":3,
        "creationTime":1533719558,
        "statut":"D\u00c9BUT DE LA CONSULTATION"
    },
    {
        "idconsultation":3,
        "creationTime":1533718082,
        "statut":"ARRIV\u00c9E DU PATIENT"
    }
]
```










