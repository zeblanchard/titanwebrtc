<!DOCTYPE html>
<?php
    if (isset($_GET["device_type"])) {
        $deviceType = $_GET["device_type"];
    } else {
        $deviceType = "desktop";
    }
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>T&eacute;l&eacute;consultation TITAN</title>

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
    <style>
        body {
            /*display: flex;*/
            min-height: 100vh;
            flex-direction: column;
        }
        main {
            /*flex: 1 0 auto;*/
        }
        .container { width: 90%;}
        .container.master { padding: 1%; }
        .fa {
            display: inline-block !important;
        }
        footer {  margin-top: 0 !important; }
        #remote {
            position: relative !important;
            /*margin: 25px 0;*/
        }
        #command form input[type="texte"] {
            display: block;
            border: 2px solid rgb(74, 74, 74);
            padding: 3px 20px;
            margin-bottom: 10px;
            border-radius: 6px;

        }
        .container.formu #hangup,
        .container.formu #call {
            background: transparent;
            border: none;
        }
    </style>
    <script src="https://cloud.apizee.com/apiRTC/apiRTC-latest.min.debug.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
<nav class="red darken-4" id="navbar">
    <div class="nav-wrapper">
        <ul id="nav-mobile" class="right">
            <li><input style="height:auto;" id="hangup" class="btn red" type="button" disabled="true" value="Raccrocher" /></li>
            <li><a class="modal-trigger" href="#modalSettings"><i class="material-icons">settings</i></a></li>
            <li><a href="#" id="audioMute"><i class="material-icons" id="audioMuteIcon">mic_off</i></a></li>
            <li><a href="#" id="videoMute"><i class="material-icons" id="videoMuteIcon">visibility_off</i></a></li>
        </ul>
    </div>
</nav>

<?php if ($deviceType == "mobile") { ?>
    <div id="mini" style="width:100%;"></div>
<?php } ?>

<main class="container grey lighten-5">

    <!-- Modal non implémentée -->
    <div id="modalUnimplement" class="modal modal-close">
        <div class="modal-content">
            <p style="text-align: center;">Fonction non implémentée pour le moment</p>
        </div>
        <div class="modal-footer">
            <a href="#" class="modal-close btn red">Fermer</a>
        </div>
    </div>
    <!-- Modal Settings -->
    <div id="modalSettings" class="modal modal-close">
        <div class="modal-content">
            <h4>Paramètres</h4>
            <!--SELECT_MEDIA-->
            <label for="audioSource">Source audio :</label>
            <select class="browser-default" id='audioSource'>
                <option value="" disabled selected>Sélectionner...</option>
            </select>
            <label for="videoSource">Source vidéo :</label>
            <select class="browser-default" id='videoSource'>
                <option value="" disabled selected>Sélectionner...</option>
            </select>
            <label for="audioOutputDevice">Sortie audio :</label>
            <select class="browser-default" id='audioOutputDevice'>
                <option value="" disabled selected>Sélectionner...</option>
            </select>
            <label for="facingMode">Caméra (pour plateforme mobile) :</label>
            <select class="browser-default" id='facingMode'>
                <option value="" disabled selected>Sélectionner...</option>
            </select>
            <!--SELECT_MEDIA-->
        </div>
        <div class="modal-footer">
            <input id="updateMediaDeviceOnCall" class="btn green modal-close" type="button" value="Ok" />
            <a href="#" class="modal-close waves-effect waves-green btn-flat">Fermer</a>
        </div>
    </div>

    <div class="container master grey lighten-5">
            <?php if ($deviceType == "desktop") { ?>
                <div id="remote" style="width:100%;"></div>
                <div id="mini" class="hide-on-med-and-down"></div>
            <?php } ?>
            <br>
        <div class="row" id="actionForm">
            <h4>Téléconsultation TITAN</h4>
            <div class="col s12 m12 l12">
                <div class="col s12 m6 l6">
                    <div class="input-field" style="display:inline-block;">
                        <input type="text" id="number" class="form-control" placeholder="Numéro de salle" aria-describedby="sizing-addon1">
                        <button type="button" id="callVideo" class="btn green btn-success">
                            <i class="fa fa-phone"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="statusCall">
            <div class="col s12 m12 l12">
                <?php //if ($deviceType == "desktop") { ?>
                    <span id="status"></span>
                    <span id="status2"></span>
                <?php //} ?>
            </div>
        </div>
        <div class="hidden" style="visibility: hidden;">
            <div id="hangupButtons"></div>
        </div>
        <br>
    </div><!-- /.container -->
</main>

<!-- Compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js"></script>
<script>
    //Initialisation des modals
    $(document).ready(function(){
        $('.modal-trigger').leanModal();
    });

    'use strict';
    var session = null,
        webRTCClient = null,
        connectedUsersList = [],

        //SELECT_MEDIA
        audioSelect = document.querySelector("select#audioSource"),
        videoSelect = document.querySelector("select#videoSource"),
        audioOutputDeviceSelect = document.querySelector("select#audioOutputDevice"),
        facingModeSelect = document.querySelector("select#facingMode"),
        callId = 0,
        deviceType = "<?php echo $deviceType ?>";

    function hideAll() {
        $("#navbar").hide();
        // $("#mini").hide();
        // $("#actionForm").hide();
        // $("#statusCall").hide();

        // let element = document.getElementById('remote');
        // element.setAttribute("style","width:100%;");
        //element.setAttribute("style","position:absolute; top:0px; left:-13%; float:left;");

        // if (element.requestFullscreen) {
        //     element.requestFullscreen();
        // }
        // else if (element.mozRequestFullScreen) {
        //     element.mozRequestFullScreen();
        // }
        // else if (element.webkitRequestFullScreen) {
        //     element.webkitRequestFullScreen();
        // }
    }

    function showAll() {
        $("#navbar").show();
        // $("#mini").show();
        // $("#actionForm").show();
        // $("#statusCall").show();

        // let element = document.getElementById('remote');
        // element.setAttribute("style","width:100%;");
        //
        // if (document.exitFullscreen) {
        //     document.exitFullscreen();
        // }
        // else if (document.mozCancelFullScreen) {
        //     document.mozCancelFullScreen();
        // }
        // else if (document.webkitCancelFullScreen) {
        //     document.webkitCancelFullScreen();
        // }
    }

    function getCallId() {
        return callId;
    }

    function setFacingModeOption(option, text) {
        var optionElt = document.createElement("option");
        optionElt.value = option;
        optionElt.text = text;
        facingModeSelect.appendChild(optionElt);
    }
    setFacingModeOption("user", "user - Front Camera" );
    setFacingModeOption("environment", "environment - Back Camera");
    setFacingModeOption("deactivated", "deactivated - Default Camera" );

    function selectMediaChange() {
        console.log('selectMediaChange');
        //webRTCClient.setAudioSourceId(audioSelect.value);
        //Storing audioSourceId in localStorage
        localStorage.setItem("audioSourceId_" + apiRTC.session.apiCCId, audioSelect.value);
        webRTCClient.setVideoSourceId(videoSelect.value);
        //Storing videoSourceId in localStorage
        localStorage.setItem("videoSourceId_" + apiRTC.session.apiCCId, videoSelect.value);
    }

    function selectOutputDeviceChange() {
        var element = null,
            divElement = null;

        console.log('selectOutputDeviceChange');
        webRTCClient.setAudioOutputId(audioOutputDeviceSelect.value);
        console.log('audioOutputDeviceSelect.value :', audioOutputDeviceSelect.value);
        //Storing videoSourceId in localStorage
        localStorage.setItem("audioOutputId_" + apiRTC.session.apiCCId, audioOutputDeviceSelect.value);

        console.log('callId :', callId);

        //Setting audio output on established call element
        if (callId !== 0) {

            element = document.getElementById('remoteElt-' + callId);
            if (element !== null) {
                setAudioOutputOnElement(element);
            }
        }
    }

    function selectfacingModeChange() {
        console.log('selectfacingModeChange :', facingModeSelect.value);
        webRTCClient.setVideoFacingMode(facingModeSelect.value)
    }

    audioSelect.onchange = selectMediaChange;
    videoSelect.onchange = selectMediaChange;
    audioOutputDeviceSelect.onchange = selectOutputDeviceChange;
    facingModeSelect.onchange = selectfacingModeChange;

    function gotSources(sourceInfos) {
        console.log('gotSources');
        var audioDeviceFound = false,
            videoDeviceFound = false,
            audioOutputDeviceFound = false,
            i = 0,
            audioSourceIdInLocalStorage = localStorage.getItem('audioSourceId_' + apiRTC.session.apiCCId),
            videoSourceIdInLocalStorage = localStorage.getItem('videoSourceId_' + apiRTC.session.apiCCId),
            audioOutputIdInLocalStorage = localStorage.getItem('audioOutputId_' + apiRTC.session.apiCCId),
            selectors = [audioSelect, audioOutputDeviceSelect, videoSelect];

        //Cleaning selectors
        selectors.forEach(function(select) {
            while (select.firstChild) {
                select.removeChild(select.firstChild);
            }
        });

        if (sourceInfos !== null) {

            for (i = 0; i != sourceInfos.length; i += 1) {

                var sourceInfo = sourceInfos[i];
                console.log('sourceInfo :', sourceInfo);

                var option = document.createElement("option");
                option.value = sourceInfo.deviceId;

                console.log('option.value :', option.value);

                //ENUMERATE
                if (sourceInfo.kind === 'audio' || sourceInfo.kind === 'audioinput') {
                    //ENUMERATE
                    console.log('audio');
                    option.text = sourceInfo.label || 'microphone ' + (audioSelect.length);
                    audioSelect.appendChild(option);
                    //ENUMERATE
                } else if (sourceInfo.kind === 'video' || sourceInfo.kind === 'videoinput') {
                    //ENUMERATE
                    console.log('video');
                    option.text = sourceInfo.label || 'camera ' + (videoSelect.length);
                    videoSelect.appendChild(option);

                } else if (sourceInfo.kind === 'audiooutput') {

                    console.log('audiooutput');
                    option.text = sourceInfo.label || 'speaker ' + (audioOutputDeviceSelect.length);
                    audioOutputDeviceSelect.appendChild(option);

                } else {
                    console.log('Some other kind of source: ', sourceInfo);
                }
                if (sourceInfo.deviceId === audioSourceIdInLocalStorage) {
                    audioDeviceFound = true;
                }
                if (sourceInfo.deviceId === videoSourceIdInLocalStorage) {
                    videoDeviceFound = true;
                }
                if (sourceInfo.deviceId === audioOutputIdInLocalStorage) {
                    audioOutputDeviceFound = true;
                }
            }
        } else {
            console.log("Media detection is not supported by browser");
        }

        if (audioDeviceFound) {
            console.log("setting with localStorage audioSourceId value");
            audioSelect.value = audioSourceIdInLocalStorage;
            //webRTCClient.setAudioSourceId(audioSourceIdInLocalStorage);
        } else {
            //removing audioSourceId from localStorage
            console.log("removing audioSourceId_ in localStorage");
            localStorage.removeItem("audioSourceId_" + apiRTC.session.apiCCId);
        }

        if (videoDeviceFound) {
            console.log("setting with localStorage videoSourceId value");
            videoSelect.value = videoSourceIdInLocalStorage;
            webRTCClient.setVideoSourceId(videoSourceIdInLocalStorage);
        } else {
            //removing videoSourceId from localStorage
            console.log("removing videoSourceId_ in localStorage");
            localStorage.removeItem("videoSourceId_" + + apiRTC.session.apiCCId);
        }

        if (audioOutputDeviceFound) {
            console.log("setting with localStorage audioOutputId value");
            audioOutputDeviceSelect.value = audioOutputIdInLocalStorage;
            webRTCClient.setAudioOutputId(audioOutputIdInLocalStorage);
        } else {
            //removing videoSourceId from localStorage
            console.log("removing audioOutputId_ in localStorage");
            localStorage.removeItem("audioOutputId_" + + apiRTC.session.apiCCId);
        }
    }
    //SELECT_MEDIA

    function setAudioOutputOnElement (mediaElt) {
        if (typeof audioOutputDeviceSelect.value !== 'undefined' && audioOutputDeviceSelect.value !== '') {

            console.log('audioOutputId set to: ' + audioOutputDeviceSelect.value);
            mediaElt.setSinkId(audioOutputDeviceSelect.value)
                .then(function () {
                    console.log('Success, audio output device attached');
                })
                .catch(function(error){
                    console.log('Error :', error);
                });

        } else {
            console.log('No audioOutputId set');
        }
    }

    function addStreamInDiv (stream, streamType, divId, mediaEltId, style, muted) {
        var mediaElt = null,
            divElement = null,
            audioOutputDeviceSelect = document.querySelector("select#audioOutputDevice");

        if (streamType === 'audio') {
            mediaElt = document.createElement("audio");
        } else {
            mediaElt = document.createElement("video");
        }
        mediaElt.id = mediaEltId;
        mediaElt.autoplay = true;
        mediaElt.muted = muted;
        if (divId != "mini" && deviceType == 'desktop') {
            mediaElt.controls = true;
        }
        mediaElt.style.width = style.width;
        mediaElt.style.height = style.height;

        //Patch for ticket on Chrome 61 Android : https://bugs.chromium.org/p/chromium/issues/detail?id=769148
        if (apiRTC.browser === 'Chrome' && apiRTC.browser_major_version === '61' && apiRTC.osName === "Android") {
            mediaElt.style.borderRadius = "1px";
            console.log('Patch for video display on Chrome 61 Android');
        }

        divElement = document.getElementById(divId);
        divElement.appendChild(mediaElt);

        if (stream !== undefined) {
            webRTCClient.attachMediaStream(mediaElt, stream);
        }

        setAudioOutputOnElement(mediaElt);
    };

    //Function to remove media stream element
    function removeElementFromDiv(divId, eltId) {
        var element = null,
            divElement = null;

        element = document.getElementById(eltId);
        if (element !== null) {
            console.log('Removing video element with Id : ' + eltId);
            divElement = document.getElementById(divId);
            divElement.removeChild(element);
        }
    }

    function initMediaElementState(callId) {
        //function that remove media element on hangup

        //You can decide to manage your own stream display function or use the integrated one of ApiRTC
        /*
            removeElementFromDiv('mini', 'miniElt-' + callId)
            removeElementFromDiv('remote', 'remoteElt-' + callId)
        */
        webRTCClient.removeElementFromDiv('mini', 'miniElt-' + callId);
        webRTCClient.removeElementFromDiv('remote', 'remoteElt-' + callId);
    }

    function addHangupButton(callId) {
        var hangupButtons = document.getElementById("hangupButtons"),
            input = document.createElement("input");

        input.setAttribute("id", "hangup-" + callId);
        input.setAttribute("value", "hangup-" + callId);
        input.setAttribute("type", "button");
        input.setAttribute("onclick", "webRTCClient.hangUp(" + callId + " );");
        hangupButtons.appendChild(input);
    }

    function removeHangupButton(callId) {
        var hangupButtonId = 'hangup-' + callId,
            hangupButton = document.getElementById(hangupButtonId),
            hangupButtons = null;

        if (hangupButton !== null) {
            console.log('Removing hangUpButton with Id : ' + hangupButtonId);
            hangupButtons = document.getElementById("hangupButtons");
            hangupButtons.removeChild(hangupButton);
        }
    }

    function userMediaErrorHandler(e) {
        $("#call").attr("disabled", false).val("Call");
        $("#hangup").attr("disabled", false).val("Raccrocher");
        alert(e.error);
    }

    //UpdateMediaDeviceOnCall
    function userMediaStopHandler(e) {
        console.log('userMediaStopHandler :', e.detail);
        webRTCClient.removeElementFromDiv('mini', 'miniElt-' + e.detail.callId);
    }
    function remoteStreamRemovedHandler(e) {
        console.log("remoteStreamRemovedHandler, e.detail :" + e.detail);
        webRTCClient.removeElementFromDiv('remote', 'remoteElt-' + e.detail.callId);
    }
    //UpdateMediaDeviceOnCall

    function hangupHandler(e) {
        console.log('hangupHandler :' + e.detail.callId);

        if (e.detail.lastEstablishedCall === true) {
            $("#call").attr("disabled", false).val("Call");
        }
        console.log(e.detail.reason);

        initMediaElementState(e.detail.callId);
        removeHangupButton(e.detail.callId);
        callId = 0;
    }

    function incomingCallHandler(e) {
        $("#hangup").attr("disabled", false).val("Raccrocher");
        addHangupButton(e.detail.callId);
        callId = e.detail.callId;
    }

    function userMediaSuccessHandler(e) {
        console.log("userMediaSuccessHandler e.detail.callId :" + e.detail.callId);
        console.log("userMediaSuccessHandler e.detail.callType :" + e.detail.callType);
        console.log("userMediaSuccessHandler e.detail.remoteId :" + e.detail.remoteId);

        //Adding local Stream in Div. Video is muted

        //You can decide to manage your own stream display function or use the integrated one of ApiRTC
        <?php if($deviceType=="mobile") { ?>
        addStreamInDiv(e.detail.stream, e.detail.callType, "mini", 'miniElt-' + e.detail.callId,
            {width : "100%", height : "auto"}, true);
        <?php } else { ?>
        addStreamInDiv(e.detail.stream, e.detail.callType, "mini", 'miniElt-' + e.detail.callId,
            {width : "160px", height : "120px"}, true);
        <?php } ?>
        /*
            webRTCClient.addStreamInDiv(e.detail.stream, e.detail.callType, "mini", 'miniElt-' + e.detail.callId,
                    {width: "128px", height: "96px"}, true);
        */

        //SAFARI : media devices list can be updated after a userMediaSuccess
        if ((apiRTC.browser === 'IE') && (apiRTC.browser_version > 8.0)) {
            apiRTC.getMediaDevicesWithCB(gotSources);
        } else {
            apiRTC.getMediaDevices()
                .then(function (sources) {
                    console.log("getMediaDevices(), sources :", sources);
                    gotSources(sources);
                })
                .catch(function (err) {
                    console.log("getMediaDevices(), error :", err);
                });
        }
    }

    function remoteStreamAddedHandler(e) {
        console.log("remoteStreamAddedHandler, e.detail.callId :" + e.detail.callId);
        console.log("remoteStreamAddedHandler, e.detail.callType :" + e.detail.callType);
        console.log("userMediaSuccessHandler e.detail.remoteId :" + e.detail.remoteId);

        //Adding Remote Stream in Div. Video is not muted
        //You can decide to manage your own stream display function or use the integrated one of ApiRTC
        <?php if ($deviceType != "mobile") { ?>
        addStreamInDiv(e.detail.stream, e.detail.callType, "remote", 'remoteElt-' + e.detail.callId,
            {width : "100%", height : "auto"}, false);
        <?php } ?>
    }

    function mediaDeviceChangedHandler(e) {
        console.log("mediaDeviceChangedHandler");
        apiRTC.getMediaDevices()
            .then(function (sources) {
                console.log("getMediaDevices(), sources :", sources);
                gotSources(sources);
            })
            .catch(function (err) {
                console.log("getMediaDevices(), error :", err);
            });
    }

    //sessionReadyHandler : apiRTC is now connected
    function sessionReadyHandler(e) {

        console.log('sessionReadyHandler:' + apiRTC.session.apiCCId);

        $("#call").attr("disabled", false).val("Call");   //Modification of Call button when phone is registered on Apizee

        apiRTC.addEventListener("userMediaSuccess", userMediaSuccessHandler);
        apiRTC.addEventListener("incomingCall", incomingCallHandler);
        apiRTC.addEventListener("userMediaError", userMediaErrorHandler);
        //UpdateMediaDeviceOnCall
        apiRTC.addEventListener("userMediaStop", userMediaStopHandler);
        apiRTC.addEventListener("remoteStreamRemoved", remoteStreamRemovedHandler);
        //UpdateMediaDeviceOnCall
        apiRTC.addEventListener("hangup", hangupHandler);
        apiRTC.addEventListener("remoteStreamAdded", remoteStreamAddedHandler);


        apiRTC.addEventListener("mediaDeviceChanged", mediaDeviceChangedHandler);

        //webRTC Client creation
        webRTCClient = apiRTC.session.createWebRTCClient({
            status: "status" //Optionnal
        });

        //SELECT_MEDIA
        //webRTCClient.getMediaDevices(gotSources);
        if ((apiRTC.browser === 'IE') && (apiRTC.browser_version > 8.0)) {
            apiRTC.getMediaDevicesWithCB(gotSources);
        } else {
            apiRTC.getMediaDevices()
                .then(function (sources) {
                    console.log("getMediaDevices(), sources :", sources);
                    gotSources(sources);
                })
                .catch(function (err) {
                    console.log("getMediaDevices(), error :", err);
                });
        }
        //SELECT_MEDIA

        //Force MCU (recorded call)
        webRTCClient.setMCUConnector('mcu4.apizee.com');

        //Multi calls Activation
        webRTCClient.setAllowMultipleCalls(true);

        //Bandwitdh limitation
        webRTCClient.setVideoBandwidth(300);

        //Call establishment
        $("#callVideo").click(function () {
            $("#hangup").attr("disabled", false).val("Raccrocher");
            callId = webRTCClient.call($("#number").val());

            console.log("callId on call =" + callId);
            if (callId != null) {
                addHangupButton(callId);
            }
        });

        //Global hangup management : all established client calls will be hangup
        $("#hangup").click(function () {
            $("#call").attr("disabled", false).val("Call");
            webRTCClient.hangUp();
        });

        //UpdateMediaDeviceOnCall
        $("#updateMediaDeviceOnCall").click(function () {
            console.log("updateMediaDeviceOnCall");
            webRTCClient.updateMediaDeviceOnCall(callId);
        });
        //UpdateMediaDeviceOnCall

        //audioMute
        $("#audioMute").click(function(e) {
            e.preventDefault();
            console.log("audioMute");
            webRTCClient.toggleAudioMute();
            if (webRTCClient.isAudioMuted()) {
                document.getElementById("audioMute").setAttribute("style","background-color:red;");
                // document.getElementById("audioMuteIcon").innerText = "mic"
            } else {
                document.getElementById("audioMute").removeAttribute("style");
                // document.getElementById("audioMuteIcon").innerText = "mic_off"
            }
        });

        //videoMute
        $("#videoMute").click(function(e) {
            e.preventDefault();
            console.log("videoMute");
            webRTCClient.toggleVideoMute();
            if (webRTCClient.isVideoMuted()) {
                document.getElementById("videoMute").setAttribute("style","background-color:red;");
                // document.getElementById("videoMuteIcon").innerText = "visibility"
            } else {
                document.getElementById("videoMute").removeAttribute("style");
                // document.getElementById("videoMuteIcon").innerText = "visibility_off"
            }
        });

        $("#")
    }

    //apiRTC initialization
    apiRTC.init({
        apiKey: "myDemoApiKey",
        //apiCCId : "1",
        onReady: sessionReadyHandler
    });
</script>
</body>
</html>
