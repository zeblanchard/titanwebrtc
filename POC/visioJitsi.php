<!DOCTYPE html>
<?php
if (isset($_GET["device_type"])) {
    $deviceType = $_GET["device_type"];
} else {
    $deviceType = "desktop";
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <title>T&eacute;l&eacute;consultation TITAN</title>
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css">
</head>
<body>
<nav class="red darken-4" id="navbar">
    <div class="nav-wrapper">
        <ul id="nav-mobile" class="right">
            <li><input style="height:auto;" id="hangup" class="btn red" type="button" value="Raccrocher" /></li>
        </ul>
    </div>
</nav>
<div class="container">
    <h4>Téléconsultation TITAN</h4>
    <div id="meet"></div>
</div>

<script type="text/javascript" src="js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<!--<script src="https://visio.titanweb.fr/libs/external_api.min.js"></script>-->
<!--<script src="https://meet.jit.si/external_api.js"></script>-->
<script type="text/javascript" src="js/external_api.js"></script>
<script>
    var domain = "visio.titanweb.fr";
    var options = {
        roomName: "ddt",
        width: "100%",
        height: 700,
        parentNode: document.querySelector("#meet"),
    };
    var api = new JitsiMeetExternalAPI(domain, options);

    $("#hangup").click(function(e) {
        e.preventDefault();
        api.executeCommand("hangup");
        api.executeCommand("dispose");
        $("#meet").hide();
    })


</script>
</body>
</html>
