//http://localhost/RD/Clients-ApiRTC/Malta-Info/Integration/conferencing_basic/?room=testMalta

$(function () {
    'use strict';

    apiRTC.setLogLevel(10);

    function displayUAInfos() {
        console.info("browser :", apiRTC.browser);
        console.info("browser_major_version :", apiRTC.browser_major_version);
        console.info("browser_version :", apiRTC.browser_version);
        console.info("osName :", apiRTC.osName);
    }

    displayUAInfos();

    //Query parameter management
    var urlParams = function () {
        var queryString = {},
            query = window.location.search.substring(1),
            vars = query.split("&"),
            i = 0;

        for (i = 0; i < vars.length; i += 1) {

            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof queryString[pair[0]] === "undefined") {
                queryString[pair[0]] = pair[1];
                // If second entry with this name
            } else if (typeof queryString[pair[0]] === "string") {
                var arr = [queryString[pair[0]], pair[1]];
                queryString[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                queryString[pair[0]].push(pair[1]);
            }
        }
        return queryString;
    }();

    console.log("urlParams", urlParams);

    var conferenceName = urlParams.room,
        localStream = null,
        ua = null,
        connectedConversation = null,
        whiteBoardClient = null,
        connectedSession = null;

    var idSnapshot = 0;
    var idImgEnCours = 0;
    var selectCamera = document.getElementById("select-camera");
    var selectMic = document.getElementById("select-mic");
    var selectedAudioInputId = null;
    var selectedVideoInputId = null;

    selectCamera.onchange = function (e) {
        console.log("selectCamera onchange");
        localStorage.setItem("videoSourceId_" + connectedSession.getId(), selectCamera.value);
    };
    selectMic.onchange = function (e) {
        console.log("selectMic onchange");
        localStorage.setItem("audioSourceId_" + connectedSession.getId(), selectMic.value);
    };

    function updateDeviceList(res) {
        var cameras = [],
            microphones = [],
            option = null,
            selectors = [selectCamera, selectMic],
            i = 0,
            v = 0;

        //Cleaning selectors
        selectors.forEach(function (select) {
            while (select.firstChild) {
                select.removeChild(select.firstChild);
            }
        });

        console.log('fetchUserMediaDevices', (res));
        selectedVideoInputId = localStorage.getItem("videoSourceId_" + connectedSession.getId());

        for (i = 0; i < Object.values(res.videoinput).length; i++) {
            v = Object.values(res.videoinput)[i];
            console.log('getCameras', v);
            option = document.createElement("option");
            option.text = v.label || 'camera ' + (cameras.length + 1);
            option.value = v.id;
            selectCamera.appendChild(option);
            cameras.push(v);

            if (v.id === selectedVideoInputId && selectedVideoInputId !== null) {
                console.log("select caméra");
                selectCamera.value = selectedVideoInputId;
            }
        }

        selectedAudioInputId = localStorage.getItem("audioSourceId_" + connectedSession.getId());
        for (i = 0; i < Object.values(res.audioinput).length; i++) {
            v = Object.values(res.audioinput)[i];
            console.log('getMicrophones', v);
            option = document.createElement("option");
            option.text = v.label || 'micro ' + (microphones.length + 1);
            option.value = v.id;
            selectMic.appendChild(option);
            microphones.push(v);

            if (v.id === selectedAudioInputId && selectedAudioInputId !== null) {
                console.log("select audio");
                selectMic.value = selectedAudioInputId;
            }
        }
        console.log('getDevices', cameras, microphones);

        localStorage.setItem("videoSourceId_" + connectedSession.getId(), selectCamera.value);
        localStorage.setItem("audioSourceId_" + connectedSession.getId(), selectMic.value);
    }

    function manageMediaDevices() {
        //SELECT_MEDIA
        var mediaDevices = ua.getUserMediaDevices();
        console.log("manageMediaDevices :", mediaDevices);
        updateDeviceList(mediaDevices);
    }

    function addUnpublishButton(stream, callId) {
        var unpublishButtons = document.getElementById("unpublishButtons"),
            input = document.createElement("input");

        input.setAttribute("id", "unpublish-" + callId);
        input.setAttribute("value", "unpublish-" + callId);
        input.setAttribute("type", "button");

        console.log('$("#unpublish-" + callId).length :', $("#unpublish-" + callId).length);

        if ($("#unpublish-" + callId).length === 0) {
            unpublishButtons.appendChild(input);
        } else {
            $("#unpublish-" + callId).replaceWith(input);
        }

        $("#unpublish-" + callId).click(function () {
            console.log('MAIN - Click unpublish-' + callId);
            console.log('MAIN - Click unpublish-' + stream.getId());
            console.log(callId);

            connectedConversation.unpublish(stream);
            stream.release();
            $("#local-media-" + callId).remove();
            $("#unpublish-" + callId).remove();
            $("#replacestream-" + callId).remove();
            $("#takesnapshot-" + callId).remove();
        });
    }

    function addSendDataToContactButton(contactId, streamId) {
        var sendDataButtons = document.getElementById("sendDataToContactButtons"),
            input = document.createElement("input");

        input.setAttribute("id", "sendDataToContact-" + streamId);
        input.setAttribute("value", "sendDataToContact-" + streamId);
        input.setAttribute("type", "button");
        sendDataButtons.appendChild(input);

        $("#sendDataToContact-" + streamId).click(function () {
            console.log('MAIN - Click sendDataToContact-' + streamId);

            connectedSession.getContact(contactId).sendData({
                order: 'takeSnapshot',
                callId: streamId,
                senderId: connectedSession.getId()
            });
        });
    }

    function addTakeSnaphotButton(stream, callId) {
        var takeSnapshotButtons = document.getElementById("takeSnapShotButtons"),
            input = document.createElement("input");

        input.setAttribute("id", "takesnapshot-" + callId);
        input.setAttribute("value", "takesnapshot-" + callId);
        input.setAttribute("type", "button");

        if ($("#takesnapshot-" + callId).length === 0) {
            takeSnapshotButtons.appendChild(input);
        } else {
            $("#takesnapshot-" + callId).replaceWith(input);
        }

        $("#takesnapshot-" + callId).click(function () {
            console.log('MAIN - Click takesnapshot-' + callId);
            stream.takeSnapshot()
                .then((media) => {
                    // OK
                    console.log('MAIN - takeSnapshot media :', media);
                    $('#snapshots').append('<a href="' + media + '" target="_blank"><img src="' + media + '" /></a>');
                })
                .catch((error) => {
                    // error
                });
        });
    }

    function addReplaceStreamButton(stream, callId) {

        var replaceStreamButtons = document.getElementById("replaceStreamButtons"),
            input = document.createElement("input");

        input.setAttribute("id", "replacestream-" + callId);
        input.setAttribute("value", "replacestream-" + callId);
        input.setAttribute("type", "button");

        if ($("#replacestream-" + callId).length === 0) {
            replaceStreamButtons.appendChild(input);
        } else {
            $("#replacestream-" + callId).replaceWith(input);
        }

        $("#replacestream-" + callId).click(function () {
            console.log('MAIN - Click replacestream-' + callId);

//Create new stream
            var option = {audioInputId: selectMic.value, videoInputId: selectCamera.value};

            ua.createStream(option)
                .then(function (newstream) {

                    console.log('New stream created :', newstream);
                    var call = connectedConversation.getConversationCall(stream);
                    console.log('call :', call);

                    call.replacePublishedStream(newstream)
                        .then(function (replacedStream) {
                            console.log('stream is replaced :', replacedStream);
                            addReplaceStreamButton(newstream, call.callId);
                            addLocalStream(newstream, call.callId);
                            addUnpublishButton(newstream, call.callId);
                            addTakeSnaphotButton(stream, call.callId);

                        }).catch(function (err) {
                        console.error('create stream error', err);
                    });

                }).catch(function (err) {
                console.error('create stream error', err);
            });
        });
    }

    function addLocalStream(stream, callId) {

        $("#local-media-" + callId).remove();
        // Get media container
        var container = document.getElementById('local-container');

        // Create media element
        var mediaElement = document.createElement('video');
        mediaElement.id = 'local-media-' + callId;
        mediaElement.autoplay = true;
        mediaElement.muted = true;

        // Add media element to media container
        container.appendChild(mediaElement);

        // Attach stream
        stream.attachToElement(mediaElement);

//Modif_TEMP
//Mute du flux audio
        //TODO : A supprimer
        if (stream !== null) {
            stream.muteAudio();
        }
//Modif_TEMP
    }

    function createStream(connectedConversation, option) {
        //==============================
        // CREATE LOCAL STREAM
        //==============================
        ua.createStream(option)
            .then(function (stream) {

                console.log("stream :", stream);

                //==============================
                // PUBLISH OWN STREAM
                //==============================
                connectedConversation.publish(stream, null)
                    .then(function (stream) {
                        console.log('stream is published :', stream);

                        var call = connectedConversation.getConversationCall(stream);
                        console.log('publish::call :', call);

                        addUnpublishButton(stream, call.callId);
                        addTakeSnaphotButton(stream, call.callId);
                        addReplaceStreamButton(stream, call.callId);
                        addLocalStream(stream, call.callId);

                    }).catch(function (err) {
                    console.error('create stream error', err);
                });

            }).catch(function (err) {
            console.error('create stream error', err);
        });
    }

    function showWhiteboardFctArea() {
        document.getElementById('whiteboardFct').style.display = 'inline-block';
    }

    function hideWhiteboardFctArea() {
        document.getElementById('whiteboardFct').style.display = 'none';
    }

    function sendPhotoToContact(contactId, media) {

        var contact = connectedSession.getContact(contactId);

        if (contact !== null) {

            var fileInfo = {
                name: 'testPhoto',
                type: 'image/png-dataUrl'
            };

            contact.sendFile(
                fileInfo,
                media
            );
            contact.on('fileTransferProgress', (info) => {
                $('#file-transfer-progress').attr("style", 'width: ' + info.transferInformation.percentage + '%');
            });
        }
    }

    function processReceivedData(e) {

        console.log("processReceivedData :", e);

        if (e.content.order === 'takeSnapshot') {

            var streamId = connectedConversation.callIdToStreamId.get(e.content.callId.toString());

            apiRTC.Stream.getStream(streamId).takeSnapshot()
                .then((media) => {
                    // OK
                    console.log('MAIN - takeSnapshot media :', media);
                    $('#snapshots').append('<a href="' + media + '" target="_blank"><img src="' + media + '" /></a>');

                    sendPhotoToContact(e.content.senderId, media);

                })
                .catch((error) => {
                    // error
                    console.error('Error :', error);
                });
        }
    }

    /**
     * Create and join a new conference
     * @author Apizee
     *
     * @param {string} name - Conference name
     * @return void
     */
    function joinConference(name) {

//TODO Creer un compte sur plateforme HDS
        var cloudUrl = 'https://cloud.apizee.com';
        var subscribedStreams = {};
        var contact = null;

        //==============================
        // CREATE USER AGENT
        //==============================
        ua = new apiRTC.UserAgent({

//TODO Creer un compte sur plateforme HDS
            uri: 'apzkey:myDemoApiKey'
        });

        ua.fetchNetworkInformation(cloudUrl)
            .then((e) => {
                console.error("fetchNetworkInformation OK", e);
            }).catch(() => {
            console.log("fetchNetworkInformation NOK");
        });

        //SELECT_MEDIA
        ua.on("mediaDeviceChanged", function (updatedContacts) {
            console.log("mediaDeviceChanged");
            manageMediaDevices();
        });

        //==============================
        // REGISTER
        //==============================
        ua.register({
            cloudUrl: cloudUrl
            //PRESENCE GROUP
            //groups: consultationGroup,
            //subscribeTo : consultationGroup,

        }).then(function (session) {
            // Save session
            connectedSession = session;

            connectedSession
                .on("contactListUpdate", function (updatedContacts) { //display a list of connected users
                    //console.error("MAIN - contactListUpdate", updatedContacts);
                })
                .on("contactData", function (e) {
                    //console.error("MAIN - contactData", e);

                    //console.error("MAIN - contactData", e.content);
                    processReceivedData(e);

                })
                .on('fileTransferInvitation', function (invitation) {
                    console.log("MAIN - fileTransferInvitation");
                    invitation.accept()
                        .then((fileObj) => {
                            console.log("MAIN - fileTransferInvitation file received");
                            console.error("fileObj :", fileObj);
                            console.log("name :", fileObj.name);
                            console.log("type :", fileObj.type);

                            $('#remotesnapshots').append('<a class="snapshot" id="' + fileObj.name + '-' + idSnapshot + '" href="#"><img id="imgSnapshot-'+idSnapshot+'" src="' + fileObj.file + '" /></a>');
                            $('#' + fileObj.name + '-' + idSnapshot).click(function (e) {
                                e.preventDefault();

                                console.error("click on snapshot :", this.id);
                                var paper = document.getElementById('paper');
                                //Exemple avec une photo accesible sur cloud.apizee.com
                                var bkgPhoto = fileObj.file;
                                paper.style.backgroundImage = 'url(' + bkgPhoto + ')';
                                paper.width = 640;
                                paper.height = 480;
                                paper.style.width = "640px";
                                paper.style.height = "480px";

                                var img = new Image();
                                img.src = fileObj.file;
                                var canvas = document.getElementById("paper");
                                var context = canvas.getContext ? canvas.getContext('2d') : null;

                                idImgEnCours = this.id;
                                context.drawImage(img, 0, 0, canvas.width, canvas.height);
                            });

                            idSnapshot++;
                        });
                });

            manageMediaDevices();

            //==============================
            // CREATE CONVERSATION
            //==============================
            connectedConversation = connectedSession.getConversation(name);

            //==============================
            // JOIN CONVERSATION
            //==============================
            connectedConversation.join()
                .then(function (response) {

                    // console.log("connectedConversation.getContacts():", connectedConversation.getContacts());

                    var option = {audioInputId: selectMic.value, videoInputId: selectCamera.value};
                    createStream(connectedConversation, option);

                    //==========================================================
                    // WHEN NEW STREAM IS AVAILABLE IN CONVERSATION
                    //==========================================================
                    connectedConversation.on('availableStreamsUpdated', function (streams) {

                        console.log('availableStreamsUpdated::streams : ', streams);

                        var keys = Object.keys(streams),
                            i = 0,
                            len = 0;

                        for (i = 0, len = keys.length; i < len; i++) {

                            //streams.isRemote : test si stream distant pour affichage ou non
                            if (streams[keys[i]].isRemote === true && typeof subscribedStreams[keys[i]] === 'undefined') {
                                //==============================
                                // SUBSCRIBE TO STREAM
                                //==============================
                                subscribedStreams[keys[i]] = streams[keys[i]];
                                connectedConversation.subscribeToMedia(keys[i]);
                            }
                        }
                    });

                    //==========================================================
                    // WHEN NEW STREAM IS ADDED TO CONVERSATION
                    //==========================================================
                    connectedConversation.on('streamAdded', function (stream) {
                        // Get remote media container
                        var container = document.getElementById('remote-container');

                        // Create media element
                        var mediaElement = document.createElement('video');
                        mediaElement.id = 'remote-media-' + stream.streamId;
                        mediaElement.autoplay = true;
                        mediaElement.muted = false;

                        // Add media element to media container
                        container.appendChild(mediaElement);

                        // Attach stream
                        stream.attachToElement(mediaElement);

                        console.log("stream.getContact().getId() :", stream.getContact().getId());
                        addSendDataToContactButton(stream.getContact().getId(), stream.getId());
                    });

                    //=====================================================
                    // WHEN STREAM WAS REMOVED FROM THE CONVERSATION
                    //=====================================================
                    connectedConversation.on('streamRemoved', function (streamInfos) {
                        console.log("streamRemoved");
                        document.getElementById('remote-media-' + streamInfos.streamId).remove();
                    });
                });
        });
    }

    joinConference(conferenceName);

    $("#createStream").click(function () {
        console.log('MAIN - Click createStream');
        var option = {audioInputId: selectMic.value, videoInputId: selectCamera.value};
        createStream(connectedConversation, option);
    });
    $("#createStreamAudio").click(function () {
        console.log('MAIN - Click createStreamAudio');
        var option = {audioInputId: selectMic.value, videoInputId: false};
        createStream(connectedConversation, option);
    });
    $("#muteAudioStream").click(function () {
        console.log('MAIN - Click muteAudioStream');
        if (localStream !== null) {
            localStream.muteAudio();
        }
    });
    $("#unmuteAudioStream").click(function () {
        console.log('MAIN - Click unmuteAudioStream');
        if (localStream !== null) {
            localStream.unmuteAudio();
        }
    });
    $("#muteVideoStream").click(function () {
        console.log('MAIN - Click muteVideoStream');
        if (localStream !== null) {
            localStream.muteVideo();
        }
    });
    $("#unmuteVideoStream").click(function () {
        console.log('MAIN - Click unmuteVideoStream');
        if (localStream !== null) {
            localStream.unmuteVideo();
        }
    });
    $("#leaveConversation").click(function () {
        console.log('MAIN - Click leaveConversation');
        var key = null;
        connectedConversation.leave();
        console.log(apiRTC.Stream.getLocalStreams());
        for (key of apiRTC.Stream.getLocalStreams()) {
            key.release();
            console.log(key.getId());
            $("#local-media-" + key.getId()).remove();
            $("#unpublish-" + key.getId()).remove();
        }
    });

    //==============================
    // WHITEBOARD FUNCTIONS
    //==============================
    // Click on startWhiteboard button
    $('#startOfflineWhiteboard').on('click', function () {
        console.log('startOfflineWhiteboard');
        //ua.startWhiteboard('paper', 'invisible');
        //ua.startWhiteboard('paper', '#07a3b2');
        ua.startWhiteboard('paper');
        //Getting whiteboardClient in order to be able to set UI parameters
        whiteBoardClient = ua.getWhiteboardClient();
        whiteBoardClient.setFocusOnDrawing(true);
        showWhiteboardFctArea();
    });
    // Click on stopWhiteboard button
    $('#stopOfflineWhiteboard').on('click', function () {
        console.log('stopOfflineWhiteboard');
        ua.stopWhiteboard();
        saveCanvasWithBackgroundImage(idImgEnCours);
        hideWhiteboardFctArea();
    });
    $('#touchScreen').on('click', function () {
        console.log('touchScreen');
        whiteBoardClient.toggleTouchScreen();
    });
    $('#clearPaper').on('click', function () {
        console.log('clearPaper');
        whiteBoardClient.deleteHistory();
    });
    $('#drawingTool').change(function () {
        whiteBoardClient.setDrawingTool($('#drawingTool').val());
    });
    $('#brushSize').change(function () {
        whiteBoardClient.setBrushSize($('#brushSize').val());
    });
    $('#brushColor').change(function () {
        whiteBoardClient.setBrushColor($('#brushColor').val());
    });
    $('#textInputScale').change(function () {
        whiteBoardClient.setScale($('#textInputScale').val());
    });
    $('#textInputOffsetX').change(function () {
        whiteBoardClient.setOffset($('#textInputOffsetX').val(), $('#textInputOffsetY').val());
    });
    $('#textInputOffsetY').change(function () {
        whiteBoardClient.setOffset($('#textInputOffsetX').val(), $('#textInputOffsetY').val());
    });
    $('#textInputButton').click(function () {
        whiteBoardClient.printSharedText($('#textInputX').val(), $('#textInputY').val(), $('#textInput').val(), 20);
    });
    $('#undo').click(function () {
        console.log('undo');
        whiteBoardClient.undo();
    });
    $('#redo').click(function () {
        console.log('redo');
        whiteBoardClient.redo();
    });
    $('#changeBackground').click(function () {
        console.log('changeBackground');

        var paper = document.getElementById('paper');
//Exemple avec une photo accesible sur cloud.apizee.com
        var bkgPhoto = "https://cloud.apizee.com/media/show/1/hash/9GToiQF4gVhYy8SAlvU05wE1jIOxrKLfb";
        paper.style.backgroundImage = 'url(' + bkgPhoto + ')';
    });

    //==============================
    // SAVE CANVAS WITH BACKGROUND IMAGE
    //==============================
    function saveCanvasWithBackgroundImage(imageId) {
        console.error("saveCanvas : ", imageId);
        //1. récupérer le canvas en cours (les dessins quoi)
        //2. récupérer l'image du background
        //3. dessiner le background sur un canvas
        //4. dessiner le canvas en cours sur le canvas
        //5. récupérer l'image
        //6. ???
        //7. profit
        var canvas = document.getElementById("paper");
        var context = canvas.getContext ? canvas.getContext("2d") : null;
        var canvas2 = document.createElement("canvas");
        canvas2.width = canvas.width;
        canvas2.height = canvas.height;
        var context2 = canvas2.getContext ? canvas2.getContext("2d") : null;
        var imgBg = new Image();
        imgBg.src = document.getElementById(imageId).src;

        context2.drawImage(canvas,0,0, canvas.width,canvas.height);
        context.clearRect(0,0,canvas.width,canvas.height);

        context.drawImage(imgBg, 0, 0, canvas.width, canvas.height);
        context.drawImage(canvas2, 0, 0, canvas.width, canvas.height);

        var dataURL = canvas.toDataURL();

        $('#whiteboardSave').append('<a class="snapshot" id="save" href="#"><img src="' + dataURL + '" /></a>');
    }

    //==============================
    // RECORDING FUNCTIONS
    //==============================
    $('#startConversationRecording').click(function () {
        console.log('startConversationRecording');
//TODO change usage apiRTC3 to use apiRTC4 API
        apiRTC.session.apiCCWebRTCClient.webRTCClient.MCUClient.startCompositeRecording(
            'media',
            'test_record_malta',
            connectedConversation.getName()
        );
    });
    $('#stopConversationRecording').click(function () {
        console.log('stopConversationRecording');
        apiRTC.session.apiCCWebRTCClient.webRTCClient.MCUClient.stopCompositeRecording();
    });
});